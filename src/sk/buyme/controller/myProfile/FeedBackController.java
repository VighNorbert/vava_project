package sk.buyme.controller.myProfile;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.IncorrectlyFilledData;
import sk.buyme.model.MailSender;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ResourceBundle;

public class FeedBackController extends Loggable implements Initializable {
    @FXML
    private TextArea feedbackTextArea;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void sendFeedBack(){
        String filled = validate();
        if (filled.equals("")) {
            try {
                MailSender.sendMail("buymeappconfirmation@gmail.com", "FeedBack", feedbackTextArea.getText());
            } catch (Exception e) {
                e.printStackTrace();
                getLogger().error("Email was not sent correctly");
            }
            Window.display("Správa", "Ďakujeme za spätnú väzbu", WindowType.ALERT_WINDOW);
            ViewManager.getInstance().setCenterView("myProfile");
        }
        else try {
            throw new IncorrectlyFilledData(filled);
        }catch (IncorrectlyFilledData e){
            getLogger().error("Incorrectly filled data");
            Window.display("Error", e.getMessage(), WindowType.ALERT_WINDOW);
        }
    }

    public String validate(){
        if (feedbackTextArea.getText().equals("")){
            return "Nesprávne vyplnená spätná väzba";
        }
        return "";
    }

    public void goBack(){
        ViewManager.getInstance().setCenterView("myProfile");
    }
}
