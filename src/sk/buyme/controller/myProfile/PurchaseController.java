package sk.buyme.controller.myProfile;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sk.buyme.Loggable;
import sk.buyme.controller.newPurchase.ShoppingCheckListController;
import sk.buyme.controller.newPurchase.ShoppingListAcceptedController;
import sk.buyme.controller.newPurchase.ShoppingListController;
import sk.buyme.controller.newPurchase.ShoppingOfferController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.ImageUrlException;
import sk.buyme.model.purchase.Purchase;
import sk.buyme.model.purchase.PurchaseModel;
import sk.buyme.model.purchase.PurchaseType;

import java.net.URL;
import java.util.ResourceBundle;

public class PurchaseController extends Loggable implements Initializable {

    @FXML
    private Label nameLabel;

    @FXML
    private Label shopLabel;

    @FXML
    private Button purchaseDetailButton;

    @FXML
    private Label estimatedPriceLabel;

    @FXML
    private Label estimatedProfitLabel;

    private Purchase purchase;

    @FXML
    private ImageView imgLogo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setPurchases(Purchase purchase, PurchaseType purchaseType){
        this.purchase = purchase;
        nameLabel.setText(purchase.getCustomer().getName());
        try{
            Image storeChainLogo = new Image("sk/buyme/logo/" + purchase.getStore().getStoreChain().getImg());
            if (storeChainLogo.isError()) {
                throw new ImageUrlException("Couldn't find image with given url");
            }
            imgLogo.setFitHeight(36);
            imgLogo.setImage(storeChainLogo);
        } catch (ImageUrlException e) {
            getLogger().error(e.getMessage());
        }

        shopLabel.setText(purchase.getStore().getName());
        estimatedPriceLabel.setText("Očakavaná cena: " + String.format("%.2f",purchase.getEstimatedPrice()) + " €");
        estimatedProfitLabel.setText("Očakavaný profit: " + String.format("%.2f",purchase.getEstimatedProfit()) + " €");

        if (purchaseType.equals(PurchaseType.SHOPPING_LIST) || purchaseType.equals(PurchaseType.SHOPPING_LIST_ACCEPTED)){
            estimatedPriceLabel.setText("Očakavaná celková cena: " + String.format("%.2f",purchase.getEstimatedPrice() + purchase.getEstimatedProfit()) + " €");
            updatePurchaseState();
        }

        purchaseDetailButton.setOnAction(e->{
            new PurchaseModel().loadProducts(purchase);
            switch(purchaseType){
                case SHOPPING_LIST:
                    ShoppingListController shoppingListController = (ShoppingListController) ViewManager.getInstance().getController("shoppingList");
                    shoppingListController.updateShoppingList(purchase);
                    ViewManager.getInstance().setCenterView("shoppingList");
                    break;
                case SHOPPING_CHECKLIST:
                    ShoppingCheckListController shoppingCheckListController = (ShoppingCheckListController) ViewManager.getInstance().getController("shoppingCheckList");
                    shoppingCheckListController.updateShoppingList(purchase);
                    ViewManager.getInstance().setCenterView("shoppingCheckList");
                    break;
                case SHOPPING_OFFER:
                    ShoppingOfferController shoppingOfferController = (ShoppingOfferController) ViewManager.getInstance().getController("shoppingOffer");
                    shoppingOfferController.updateShoppingOffer(purchase);
                    ViewManager.getInstance().setCenterView("shoppingOffer");
                    break;
                case SHOPPING_LIST_ACCEPTED:
                    ShoppingListAcceptedController shoppingListAcceptedController = (ShoppingListAcceptedController) ViewManager.getInstance().getController("shoppingListAccepted");
                    shoppingListAcceptedController.updateShoppingList(purchase);
                    ViewManager.getInstance().setCenterView("shoppingListAccepted");

            }
        });

    }

    private void updatePurchaseState(){
        switch (purchase.getStatus()){
            case 1:
                estimatedProfitLabel.setText("Neprijatá");
                break;
            case 2:
                estimatedProfitLabel.setText("Prijatá");
                break;
            case 3:
                estimatedProfitLabel.setText("Nakúpené");
                break;
            case 4:
                estimatedProfitLabel.setText("Doručená");
                break;
            case 5:
                estimatedProfitLabel.setText("Odstránená");
                break;
        }
    }

    public Purchase getPurchase() {
        return purchase;
    }
}
