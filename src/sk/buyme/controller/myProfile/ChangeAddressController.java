package sk.buyme.controller.myProfile;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import sk.buyme.Loggable;
import sk.buyme.controller.mainWindow.MyProfileController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.IncorrectlyFilledData;
import sk.buyme.model.MailSender;
import sk.buyme.model.address.AddressModel;
import sk.buyme.model.address.City;
import sk.buyme.model.address.District;
import sk.buyme.model.address.Region;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.User;
import sk.buyme.model.user.UserModel;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ChangeAddressController extends Loggable implements Initializable {

    @FXML
    private ComboBox<Region> regionComboBox;

    @FXML
    private ComboBox<District> districtComboBox;

    @FXML
    private ComboBox<City> cityComboBox;

    @FXML
    private TextField addressTextField;

    ObservableList<Region> regionObservableList = FXCollections.observableArrayList();
    ObservableList<District> districtObservableList = FXCollections.observableArrayList();
    ObservableList<City> cityObservableList = FXCollections.observableArrayList();

    private final AddressModel addressModel = new AddressModel();

    private static User user = new User();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ArrayList<Region> regions = addressModel.getAllRegions();
//        ArrayList<District> districts = addressModel.getDistricts(regions.get(0));
//        ArrayList<City> cities = new AddressModel().getCities(districts.get(0));
        regionObservableList.addAll(regions);
        regionComboBox.setItems(regionObservableList);
//
//        districtObservableList.addAll(districts);
//        districtComboBox.setItems(districtObservableList);
//
//        cityObservableList.addAll(cities);
//        cityComboBox.setItems(cityObservableList);
//
        districtComboBox.setDisable(true);
        cityComboBox.setDisable(true);
        addressTextField.setDisable(true);
    }

    public void changeOfComboRegion(){
        districtComboBox.setDisable(false);
        ArrayList<District> districts = addressModel.getDistricts(regionComboBox.getValue());
        districtObservableList.clear();
        districtObservableList.addAll(districts);
        districtComboBox.setItems(districtObservableList);
        cityComboBox.setDisable(true);
        addressTextField.setDisable(true);
        addressTextField.clear();
        cityObservableList.clear();
        cityComboBox.setItems(cityObservableList);
    }

    public void changeOfComboDistrict(){
        if (districtComboBox.getValue() != null) {
            cityComboBox.setDisable(false);
            ArrayList<City> cities = addressModel.getCities(districtComboBox.getValue());
            cityObservableList.clear();
            cityObservableList.addAll(cities);
            cityComboBox.setItems(cityObservableList);
            addressTextField.setDisable(true);
            addressTextField.clear();
        }
    }

    public void changeOfComboCity() {
        if (!cityComboBox.isDisabled()) {
            addressTextField.setDisable(false);
            addressTextField.clear();
        }
    }

    public void changeOfAddress() throws Exception {
        String filled = checkPlacesValidation(regionComboBox, districtComboBox, cityComboBox, addressTextField);
        if (filled.equals("")) {
            //Mail.sendMail("xkabac@stuba.sk", "Confirmation BuyMe", "Please confirm this");
            MailSender.sendMail(Application.getInstance().getLoggedInUser().getEmail(), "Zmenená adresa", "Dobrý deň<br/><br/>Vaša adresa v aplikácií BuyMe bola zmenená<br/><br/> Váš tím aplikácie BuyMe");
            Window.display("Zmena adresy", "Vaša adresa bola úspešne zmenená", WindowType.ALERT_WINDOW);
            user.setCity(cityComboBox.getValue());
            user.setStreet(addressTextField.getText());
            new UserModel().save(user, false, false);
            MyProfileController controller = (MyProfileController) ViewManager.getInstance().getController("myProfile");
            controller.setUser();
            ViewManager.getInstance().setCenterView("personalInfo");
        }
        else   try {
            throw new IncorrectlyFilledData(filled);
        } catch (IncorrectlyFilledData e) {
            getLogger().error("Incorretly filled data");
            Window.display("Error", filled, WindowType.ALERT_WINDOW);
        }
    }

    public String checkPlacesValidation(ComboBox<Region> comboBoxRegion, ComboBox<District> comboBoxDistrict, ComboBox<City> comboBoxCity, TextField textFieldAddress){
        if (comboBoxRegion.getValue() == null)
            return "Región nevyplnený";
        if (comboBoxDistrict.getValue() == null)
            return "Distrikt nevyplnený";
        if (comboBoxCity.getValue() == null)
            return "Mesto nevyplnené";
        if (textFieldAddress.getText().equals(""))
            return "Adresa a číslo domu nesprávne vyplnené";
        return "";
    }

    public void goBack(){
        ViewManager.getInstance().setCenterView("personalInfo");
    }

    public void setData(User userLogged){
        user = userLogged;
    }


}
