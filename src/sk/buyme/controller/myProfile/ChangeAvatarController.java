package sk.buyme.controller.myProfile;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import sk.buyme.controller.mainWindow.MyProfileController;
import sk.buyme.controller.surrouding.RegistrationPlaceController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.user.*;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ResourceBundle;

public class ChangeAvatarController implements Initializable {

    @FXML
    private ComboBox<String> topTextField;

    @FXML
    private ComboBox<String> accessoriesTextField;

    @FXML
    private ComboBox<String> hairColorTextField;

    @FXML
    private ComboBox<String> facialHairTypeTextField;

    @FXML
    private ComboBox<String> facialHairColorTextField;

    @FXML
    private ComboBox<String> clotheTypeTextField;

    @FXML
    private ComboBox<String> clotheColorTextField;

    @FXML
    private ComboBox<String> eyeTypeTextField;

    @FXML
    private ComboBox<String> eyebrowTypeTextField;

    @FXML
    private ComboBox<String> mouthTypeTextField;

    @FXML
    private ComboBox<String> skinColorTextField;

    @FXML
    private ImageView img;

    ObservableList<String> topObservableList = FXCollections.observableArrayList();
    ObservableList<String> accessoriesObservableList = FXCollections.observableArrayList();
    ObservableList<String> hairColorObservableList = FXCollections.observableArrayList();
    ObservableList<String> facialHairTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> clotheTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> clotheColorObservableList = FXCollections.observableArrayList();
    ObservableList<String> eyeTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> eyebrowTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> mouthTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> facialHairColorObservableList = FXCollections.observableArrayList();
    ObservableList<String> skinColorObservableList = FXCollections.observableArrayList();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        topObservableList.addAll(AvatarPossibilities.getTopType());
        topTextField.setItems(topObservableList);

        accessoriesObservableList.addAll(AvatarPossibilities.getAccessoriesType());
        accessoriesTextField.setItems(accessoriesObservableList);

        hairColorObservableList.addAll(AvatarPossibilities.getHairColor());
        hairColorTextField.setItems(hairColorObservableList);

        facialHairTypeObservableList.addAll(AvatarPossibilities.getFacialHairType());
        facialHairTypeTextField.setItems(facialHairTypeObservableList);

        clotheTypeObservableList.addAll(AvatarPossibilities.getClotheType());
        clotheTypeTextField.setItems(clotheTypeObservableList);

        clotheColorObservableList.addAll(AvatarPossibilities.getClotheColor());
        clotheColorTextField.setItems(clotheColorObservableList);

        eyeTypeObservableList.addAll(AvatarPossibilities.getEyeType());
        eyeTypeTextField.setItems(eyeTypeObservableList);

        eyebrowTypeObservableList.addAll(AvatarPossibilities.getEyebrowType());
        eyebrowTypeTextField.setItems(eyebrowTypeObservableList);

        mouthTypeObservableList.addAll(AvatarPossibilities.getMouthType());
        mouthTypeTextField.setItems(mouthTypeObservableList);

        facialHairColorObservableList.addAll(AvatarPossibilities.getFacialHairColor());
        facialHairColorTextField.setItems(facialHairColorObservableList);

        skinColorObservableList.addAll(AvatarPossibilities.getSkinType());
        skinColorTextField.setItems(skinColorObservableList);

//
    }

    public void setData(){
        topTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getTopType());
        accessoriesTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getAccessoriesType());
        hairColorTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getHairColor());
        facialHairTypeTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getFacialHairType());
        facialHairColorTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getFacialHairColor());
        clotheTypeTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getClotheType());
        clotheColorTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getClotheColor());
        eyeTypeTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getEyeType());
        eyebrowTypeTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getEyebrowType());
        mouthTypeTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getMouthType());
        skinColorTextField.setValue(Application.getInstance().getLoggedInUser().getAvatar().getSkinColor());
        AvatarViewer.showAvatar(Application.getInstance().getLoggedInUser().getAvatar(), img, null);
    }

    public void setAvatar(){
        Avatar avatar = new Avatar(topTextField.getValue(), accessoriesTextField.getValue(), hairColorTextField.getValue(),
                facialHairTypeTextField.getValue(), facialHairColorTextField.getValue(), clotheTypeTextField.getValue(),
                clotheColorTextField.getValue(), eyeTypeTextField.getValue(), eyebrowTypeTextField.getValue(),
                mouthTypeTextField.getValue(), skinColorTextField.getValue());
        User u = Application.getInstance().getLoggedInUser();
        u.setAvatar(avatar);
        new UserModel().save(u, false, true);
        Window.display("Avatar", "Avatar bol zmenený", WindowType.ALERT_WINDOW);
        MyProfileController controller = (MyProfileController) ViewManager.getInstance().getController("myProfile");
        controller.setUser();
        ViewManager.getInstance().setCenterView("myProfile");
    }

    public void updateAvatar() {
        Avatar avatar = new Avatar(topTextField.getValue(), accessoriesTextField.getValue(), hairColorTextField.getValue(),
                facialHairTypeTextField.getValue(), facialHairColorTextField.getValue(), clotheTypeTextField.getValue(),
                clotheColorTextField.getValue(), eyeTypeTextField.getValue(), eyebrowTypeTextField.getValue(),
                mouthTypeTextField.getValue(), skinColorTextField.getValue());
        AvatarViewer.showAvatar(avatar, img, null);
    }

    public void goBack() {
        ViewManager.getInstance().setCenterView("personalInfo");
    }
}
