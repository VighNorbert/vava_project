package sk.buyme.controller.myProfile;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import sk.buyme.model.user.Application;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.user.User;

import java.net.URL;
import java.util.ResourceBundle;

public class PersonalInformationController implements Initializable {

    @FXML
    private Label nameLabel;

    @FXML
    private Label usernameLabel;

    @FXML
    private Label emailLabel;

    private User user = new User();
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setData(User userLogged){
        user = userLogged;
        nameLabel.setText(user.getName());
        usernameLabel.setText(user.getUsername());
        emailLabel.setText(user.getEmail());
    }

    public void changeAvatar(){
        ChangeAvatarController controller = (ChangeAvatarController) ViewManager.getInstance().getController("changeAvatar");
        controller.setData();
        ViewManager.getInstance().setCenterView("changeAvatar");
    }


    public void changeOfPassword(){
        ChangePasswordController controller = (ChangePasswordController) ViewManager.getInstance().getController("changePassword");
        controller.setData(Application.getInstance().getLoggedInUser());
        ViewManager.getInstance().setCenterView("changePassword");
    }

    public void changeOfAddress(){
        ChangeAddressController controller = (ChangeAddressController) ViewManager.getInstance().getController("changeAddress");
        controller.setData(Application.getInstance().getLoggedInUser());
        ViewManager.getInstance().setCenterView("changeAddress");
    }

    public void goBack(){
        ViewManager.getInstance().setCenterView("myProfile");
    }
}
