package sk.buyme.controller.myProfile;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.RegistrationInfoController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.AuthenticationException;
import sk.buyme.exceptions.IncorrectlyFilledData;
import sk.buyme.exceptions.UserNotConfirmedException;
import sk.buyme.model.MailSender;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.User;
import sk.buyme.model.user.UserModel;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ResourceBundle;

public class ChangePasswordController extends Loggable implements Initializable {
    @FXML
    private PasswordField oldPasswordTextField;

    @FXML
    private PasswordField newPasswordTextField;

    @FXML
    private PasswordField newPasswordRepeatTextField;

    private User user = new User();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        oldPasswordTextField.clear();
        newPasswordTextField.clear();
        newPasswordRepeatTextField.clear();
    }


    public void setData(User userLogged){
        user = userLogged;
    }

    public void changeOfPassword(){
        String filled = validation();
        if (filled.equals("")){
            user.setPassword(newPasswordTextField.getText());
            new UserModel().save(user, true, false);
            ViewManager.getInstance().setCenterView("personalInfo");
            try {
                MailSender.sendMail(Application.getInstance().getLoggedInUser().getEmail(), "Zmenená heslo", "Dobrý deň<br/><br/>Vaše heslo v aplikácií BuyMe bolo zmenené<br/><br/> Váš tím aplikácie BuyMe");
                Window.display("Zmena hesla", "Vaša heslo bola úspešne zmenené", WindowType.ALERT_WINDOW);
            } catch (Exception e) {
                getLogger().error("Email was not sent correctly");
            }

        }
        else try{
            throw new IncorrectlyFilledData(filled);
        }
        catch (IncorrectlyFilledData e){
            getLogger().error("Incorretly filled data");
            Window.display("Error", e.getMessage(), WindowType.ALERT_WINDOW);
        }
    }

    public String validation(){
        try {
            User user2 = new UserModel().authenticate(user.getUsername(), oldPasswordTextField.getText());
        } catch (AuthenticationException authenticationException) {
            return "Nesprávne heslo";
        } catch (UserNotConfirmedException e) {
            return "Používateľ nebol zatiaľ potvrdený";
        }
        if (!RegistrationInfoController.passwordValidation(newPasswordTextField.getText())){
            return "Heslo nesprávne vyplnené(minimum 6 znakov, minimálne 1 veľké písmeno, minimálne 1 malé písmeno a 1 číslo)";
    }
        if (!RegistrationInfoController.passwordValidation(newPasswordRepeatTextField.getText())){
            return "Heslo nesprávne vyplnené(minimum 6 znakov, minimálne 1 veľké písmeno, minimálne 1 malé písmeno a 1 číslo)";
        }
        if (!RegistrationInfoController.checkPasswordsSame(newPasswordTextField, newPasswordRepeatTextField))
            return "Heslá sa nezhodujú";
        return "";
    }

    public void goBack(){
        ViewManager.getInstance().setCenterView("personalInfo");
    }

}
