package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sk.buyme.Loggable;
import sk.buyme.exceptions.ImageUrlException;
import sk.buyme.model.products.ShoppingCartItem;

import java.net.URL;
import java.util.ResourceBundle;

public class ShoppingListItemController extends Loggable implements Initializable {

    private ShoppingCartItem actualItem = null;

    @FXML
    private ImageView productImageView;

    @FXML
    private Label productNameLabel;

    @FXML
    private Label productPriceLabel;

    @FXML
    private Label totalPriceLabel;

    @FXML
    private Label quantityLabel;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setShoppingListItem(ShoppingCartItem newItem) {
        actualItem = newItem;
        productNameLabel.setText(actualItem.getProduct().getName());
        productPriceLabel.setText(String.format("%.2f", actualItem.getProduct().getPrice()) + " €/ks");
        totalPriceLabel.setText(String.format("%.2f", actualItem.getPrice()) + " €");
        quantityLabel.setText(Integer.toString(newItem.getQuantity()) + " ks");

        try{
            Image productImage = new Image(newItem.getProduct().getImageUrl());
            if (productImage.isError()) {
                throw new ImageUrlException("Couldn't find image with given url");
            }
            productImageView.setImage(productImage);
        }
        catch(ImageUrlException e){
            getLogger().error(e.getMessage());
        }
    }
}
