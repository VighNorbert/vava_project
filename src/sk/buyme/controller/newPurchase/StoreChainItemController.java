package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sk.buyme.Loggable;
import sk.buyme.exceptions.ImageUrlException;
import sk.buyme.model.store.StoreChain;

import java.net.URL;
import java.util.ResourceBundle;

public class StoreChainItemController extends Loggable implements Initializable {
    private StoreChain myStoreChain;

    @FXML
    private ImageView logoImageView;

    @FXML
    private Label StoreChainLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setStoreChain(StoreChain storeChain){
        myStoreChain = storeChain;
        StoreChainLabel.setText(storeChain.getName());

        try{
            Image storeChainLogo = new Image( "sk/buyme/logo/" + storeChain.getImg());
            if (storeChainLogo.isError()) {
                throw new ImageUrlException("Couldn't find image with given url");
            }
            logoImageView.setFitHeight(60);
            logoImageView.setImage(storeChainLogo);
        }
        catch(ImageUrlException | IllegalArgumentException e){
            getLogger().error(e.getMessage());
        }
    }
}
