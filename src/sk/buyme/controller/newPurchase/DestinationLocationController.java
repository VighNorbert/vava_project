package sk.buyme.controller.newPurchase;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import sk.buyme.Loggable;
import sk.buyme.controller.mainWindow.MyPurchasesController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.IncorrectlyFilledData;
import sk.buyme.model.address.AddressModel;
import sk.buyme.model.address.City;
import sk.buyme.model.address.District;
import sk.buyme.model.address.Region;
import sk.buyme.model.products.ShoppingCart;
import sk.buyme.model.products.ShoppingCartItem;
import sk.buyme.model.purchase.Purchase;
import sk.buyme.model.purchase.PurchaseModel;
import sk.buyme.model.store.Store;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.User;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DestinationLocationController extends Loggable implements Initializable {

    @FXML
    private ComboBox<Region> regionComboBox;

    @FXML
    private ComboBox<District> districtComboBox;

    @FXML
    private ComboBox<City> cityComboBox;

    @FXML
    private TextField addressTextField;

    ObservableList<Region> regionObservableList = FXCollections.observableArrayList();
    ObservableList<District> districtObservableList = FXCollections.observableArrayList();
    ObservableList<City> cityObservableList = FXCollections.observableArrayList();

    private final AddressModel addressModel = new AddressModel();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ArrayList<Region> regions = addressModel.getAllRegions();
        regionObservableList.addAll(regions);
        regionComboBox.setItems(regionObservableList);
        districtComboBox.setDisable(true);
        cityComboBox.setDisable(true);
        addressTextField.setDisable(true);
    }

    public void changeOfComboRegion(){
        districtComboBox.setDisable(false);
        ArrayList<District> districts = addressModel.getDistricts(regionComboBox.getValue());
        districtObservableList.clear();
        districtObservableList.addAll(districts);
        districtComboBox.setItems(districtObservableList);
        cityComboBox.setDisable(true);
        addressTextField.setDisable(true);
        addressTextField.clear();
        cityObservableList.clear();
        cityComboBox.setItems(cityObservableList);
    }

    public void changeOfComboDistrict(){
        if (districtComboBox.getValue() != null) {
            cityComboBox.setDisable(false);
            ArrayList<City> cities = addressModel.getCities(districtComboBox.getValue());
            cityObservableList.clear();
            cityObservableList.addAll(cities);
            cityComboBox.setItems(cityObservableList);
            addressTextField.setDisable(true);
            addressTextField.clear();
        }
    }

    public void changeOfComboCity() {
        if (!cityComboBox.isDisabled()) {
            addressTextField.setDisable(false);
            addressTextField.clear();
        }
    }

    public void orderComplete(){
        String filled = checkPlacesValidation(regionComboBox, districtComboBox, cityComboBox, addressTextField);
        if (filled.equals("")) {
            if(Window.display("Potvrdenie objednávky", "Naozaj chcete potvrdiť objednávku?", WindowType.CONFIRM_WINDOW)) {
                User customer = Application.getInstance().getLoggedInUser();
                Store selectedStore = Application.getInstance().getSelectedStore();
                double estimatedPrice = ShoppingCart.getInstance().getShoppingPrice();
                double estimatedProfit = Math.max(2.00, estimatedPrice * 0.15);
                ArrayList<ShoppingCartItem> shoppingCartItems = ShoppingCart.getInstance().getShoppingCartItems();

                Purchase newPurchase = new Purchase(customer, selectedStore, estimatedPrice, estimatedProfit, shoppingCartItems, addressTextField.getText(), cityComboBox.getValue());
                new PurchaseModel().save(newPurchase);
                getLogger().info("New order successfully created");

                ShoppingCart.getInstance().clearShoppingCart();
                Application.getInstance().setSelectedStore(null);

                MyPurchasesController myPurchasesController = (MyPurchasesController) ViewManager.getInstance().getController("myPurchases");
                myPurchasesController.setData();
                ViewManager.getInstance().setCenterView("myPurchases");

            }
        }
        else   try {
            throw new IncorrectlyFilledData(filled);
        } catch (IncorrectlyFilledData e) {
            getLogger().error("Incorrectly filled data");
            Window.display("Error", filled, WindowType.ALERT_WINDOW);
        }
    }

    public void setDefaultUserAddress(){
        try {
            User loggedInUser = Application.getInstance().getLoggedInUser();
            regionComboBox.getSelectionModel().select(loggedInUser.getRegion());
            districtComboBox.getSelectionModel().select(loggedInUser.getDistrict());
            cityComboBox.getSelectionModel().select(loggedInUser.getCity());
            addressTextField.setText(loggedInUser.getStreet());

            regionComboBox.setDisable(false);
            districtComboBox.setDisable(false);
            cityComboBox.setDisable(false);
            addressTextField.setDisable(false);
        }
        catch (Exception e){
            getLogger().error("Couldn't load user's address");
        }
    }

    public String checkPlacesValidation(ComboBox<Region> comboBoxRegion, ComboBox<District> comboBoxDistrict, ComboBox<City> comboBoxCity, TextField textFieldAddress){
        if (comboBoxRegion.getValue() == null)
            return "Región nevyplnený";
        if (comboBoxDistrict.getValue() == null)
            return "Distrikt nevyplnený";
        if (comboBoxCity.getValue() == null)
            return "Mesto nevyplnené";
        if (textFieldAddress.getText().equals(""))
            return "Adresa a číslo domu nesprávne vyplnené";
        return "";
    }

    public void back(){
        ViewManager.getInstance().setCenterView("shoppingCart");
    }
}
