package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.products.ShoppingCart;
import sk.buyme.model.products.ShoppingCartItem;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class ShoppingCartController extends Loggable implements Initializable {

    @FXML
    private VBox shoppingCartVBox;
    @FXML
    private Label totalPriceLabel;
    @FXML
    private Label shoppingPriceLabel;
    @FXML
    private Label deliveryPriceLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void clearShoppingCart(){
        shoppingCartVBox.getChildren().clear();
        updatePrice();
    }

    public void addShoppingCartItem(ShoppingCartItem newItem){

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShoppingCartItem.fxml"));
            Parent shoppingCartItem = loader.load();
            ShoppingCartItemController controller = loader.getController();
            controller.setShoppingCartItem(newItem);

            VBox.setMargin(shoppingCartItem, new Insets(0, 0, 20, 0));

            shoppingCartVBox.getChildren().add(shoppingCartItem);
            updatePrice();

            controller.getRemoveItemButton().setOnAction(e ->{
                if(Window.display("Odstránenie predmetu z košíka", "Ste si istý že chcete odstrániť produkt z košíka?", WindowType.CONFIRM_WINDOW)) {
                    shoppingCartVBox.getChildren().remove(shoppingCartItem);
                    sk.buyme.model.products.ShoppingCart.getInstance().removeShoppingCartItem(newItem);
                    updatePrice();
                }
            });


        }
        catch (IOException e){
            getLogger().error("Couldn't load fxml file");

        }
    }

    public void shopProductsView(){
        ViewManager.getInstance().setCenterView("shopProducts");
    }

    public void removeShoppingCartItems(){
        boolean remove = Window.display("Zrušenie nákupu", "Ste si istý že chcete zrušiť nákup?", WindowType.CONFIRM_WINDOW);
        if(remove){
            ShoppingCart.getInstance().clearShoppingCart();
            ViewManager.getInstance().setCenterView("myPurchases");
        }
    }

    public void updatePrice(){
        deliveryPriceLabel.setText(String.format("%.2f", ShoppingCart.getInstance().getDeliveryPrice()) + " €");
        shoppingPriceLabel.setText(String.format("%.2f", ShoppingCart.getInstance().getShoppingPrice()) + " €");
        totalPriceLabel.setText(String.format("%.2f", ShoppingCart.getInstance().getTotalPrice()) + " €");
    }

    public void deliveryView(){
        if(sk.buyme.model.products.ShoppingCart.getInstance().getShoppingCartItems().isEmpty()){
            Window.display("Žiadne produkty v košíku",
                    "Aby ste mohli prejsť na objednávku, je potrebné vložiť \nnejaké produkty do košíka.",
                    WindowType.ALERT_WINDOW);
            return;
        }
        else if (ShoppingCart.getInstance().getShoppingPrice() < 2.0){
            Window.display("Nedostatočná cena nákupu",
                    "Na vytvorenie nákupnej požiadavky je potrebný nákup\n v hodnote aspoň 2€",
                    WindowType.ALERT_WINDOW);
            return;
        }

        DestinationLocationController destinationController = (DestinationLocationController) ViewManager.getInstance().getController("destinationLocation");
        destinationController.setDefaultUserAddress();
        ViewManager.getInstance().setCenterView("destinationLocation");

    }
}
