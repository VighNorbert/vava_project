package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.ImageUrlException;
import sk.buyme.model.products.ShoppingCartItem;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class ShoppingCartItemController extends Loggable implements Initializable {

    private ShoppingCartItem actualItem = null;

    @FXML
    private ImageView productImageView;

    @FXML
    private Label productNameLabel;

    @FXML
    private Label productPriceLabel;

    @FXML
    private Label totalPriceLabel;

    @FXML
    private Button removeItemButton;

    @FXML
    private Spinner<Integer> quantitySpinner;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 9999));
        quantitySpinner.valueProperty().addListener((obs, oldValue, newValue)->{
            if(!newValue.equals(oldValue)){
                actualItem.setQuantity(newValue);
                totalPriceLabel.setText(String.format("%.2f", actualItem.getPrice()) + " €");

                ShoppingCartController controller = (ShoppingCartController) ViewManager.getInstance().getController("shoppingCart");
                controller.updatePrice();
            }
        });

    }

    public void setShoppingCartItem(ShoppingCartItem newItem) {
        actualItem = newItem;
        productNameLabel.setText(newItem.getProduct().getName());
        productPriceLabel.setText(String.format("%.2f", newItem.getProduct().getPrice()) + " €/ks");
        totalPriceLabel.setText(String.format("%.2f", newItem.getPrice()) + " €");
        quantitySpinner.getValueFactory().setValue(newItem.getQuantity());

        try{
            Image productImage = new Image(newItem.getProduct().getImageUrl());
            if (productImage.isError()) {
                throw new ImageUrlException("Couldn't find image with given url");
            }
            productImageView.setImage(productImage);
        }
        catch(ImageUrlException e){
            getLogger().error(e.getMessage());
        }
    }

    public Button getRemoveItemButton(){
        return removeItemButton;
    }
}
