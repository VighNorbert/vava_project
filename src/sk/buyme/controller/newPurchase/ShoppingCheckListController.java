package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.controller.mainWindow.AcceptedPurchasesController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.products.ShoppingCartItem;
import sk.buyme.model.purchase.Purchase;
import sk.buyme.model.purchase.PurchaseModel;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.AvatarViewer;
import sk.buyme.model.user.User;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ShoppingCheckListController extends Loggable implements Initializable {

    @FXML
    private VBox shoppingListVBox;
    @FXML
    private Label totalPriceLabel;
    @FXML
    private Label shoppingPriceLabel;
    @FXML
    private Label deliveryPriceLabel;
    @FXML
    private ImageView avatarImageView;
    @FXML
    private Label customerNameLabel;
    @FXML
    private Label customerAddressLabel;

    @FXML
    private Button completedShoppingButton;

    private Purchase actualPurchase;

    private ArrayList<ShoppingCartItem> unavailableItems = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void updateShoppingList(Purchase purchase){
        boolean completed = purchase.getStatus()==3;
        shoppingListVBox.getChildren().clear();
        actualPurchase = purchase;
        User customer = purchase.getCustomer();
        customerNameLabel.setText(customer.getName());
        customerAddressLabel.setText(purchase.getStreet() + ",\n" + purchase.getCity());

        AvatarViewer.showAvatar(customer, avatarImageView);
        ArrayList<ShoppingCartItem> shoppingListItems = purchase.getProducts();

        for (ShoppingCartItem listItem:
             shoppingListItems) {

            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShoppingCheckListItem.fxml"));
                Parent shoppingListItem = loader.load();
                ShoppingCheckListItemController controller = loader.getController();
                controller.setShoppingListItem(listItem, actualPurchase);

                if(completed){
                    controller.getNotAvailableCheckBox().setDisable(true);
                    controller.getPurchasedCheckBox().setDisable(true);
                    completedShoppingButton.setVisible(false);
                }
                else{
                    completedShoppingButton.setVisible(true);
                }

                VBox.setMargin(shoppingListItem, new Insets(0, 0, 20, 0));

                if (listItem.isAvailable()!=null) {
                    if(!listItem.isAvailable()) {
                        controller.setUnavailable();
                    }
                    else{
                        controller.setBought();
                    }
                }

                shoppingListVBox.getChildren().add(shoppingListItem);
            } catch (IOException e) {
                getLogger().error("Couldn't load fxml file");
//                e.printStackTrace();
            }

            updatePrice();
        }
    }

    public double calculateShoppingPrice(){
        ArrayList<ShoppingCartItem> shoppingCartItems = new ArrayList<>(actualPurchase.getProducts());
        shoppingCartItems.removeAll(unavailableItems);
        double shoppingPrice = 0d;
        for (ShoppingCartItem shoppingCartItem:
                shoppingCartItems) {
            shoppingPrice += shoppingCartItem.getPrice();
        }
        return shoppingPrice;
    }

    public void updatePrice() {
        double shoppingPrice = calculateShoppingPrice();
        double deliveryPrice = Math.max(shoppingPrice * 0.15, 2.00);
        double totalPrice = deliveryPrice + shoppingPrice;
        deliveryPriceLabel.setText(String.format("%.2f", deliveryPrice) + " €");
        shoppingPriceLabel.setText(String.format("%.2f", shoppingPrice) + " €");
        totalPriceLabel.setText(String.format("%.2f", totalPrice) + " €");
    }

    public void addUnavailableItem(ShoppingCartItem shoppingCartItem){
        unavailableItems.add(shoppingCartItem);
    }
    public void removeUnavailableItem(ShoppingCartItem shoppingCartItem){
        unavailableItems.remove(shoppingCartItem);
    }

    public void completedShopping(){
        if(Window.display("Dokončenie nákupu", "Ste si istý že chcete potvrdiť dokončenie nákupu?", WindowType.CONFIRM_WINDOW)){
            double shoppingPrice = calculateShoppingPrice();
            double deliveryPrice = Math.max(shoppingPrice * 0.15, 2.00);
            actualPurchase.finish(shoppingPrice, deliveryPrice);

            new PurchaseModel().setBought(actualPurchase);

            ((AcceptedPurchasesController)ViewManager.getInstance().getController("acceptedPurchases")).setData();
            ViewManager.getInstance().setCenterView("acceptedPurchases");
        }
    }

    public void back(){
        ViewManager.getInstance().setCenterView("acceptedPurchases");
    }

}
