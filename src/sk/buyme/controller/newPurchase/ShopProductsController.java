package sk.buyme.controller.newPurchase;

import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.ImageUrlException;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.model.products.Product;
import sk.buyme.model.products.ProductCategory;
import sk.buyme.model.products.ProductModel;
import sk.buyme.model.products.ProductSortBy;
import sk.buyme.model.store.StoreModel;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import static jdk.nashorn.internal.objects.NativeMath.min;

public class ShopProductsController extends Loggable implements Initializable {
    @FXML
    private ScrollPane scrollPane;

    @FXML
    private ComboBox<ProductCategory> categoryComboBox;

    @FXML
    private Button searchButton;

    @FXML
    private VBox mainVBox;

    @FXML
    private HBox paginationHBox;

    @FXML
    private VBox shopProductsVBox;

    @FXML
    private Button nextPageButton;

    @FXML
    private Button previousPageButton;

    @FXML
    private Label pagesLabel;

    @FXML
    private TextField searchTextField;

    @FXML
    private ComboBox<String> sortByComboBox;



    @FXML
    private Button shoppingCartButton;

    private final int PER_PAGE = 12;
    private ProductSortBy sortBy = ProductSortBy.PRICE_ASC;
    private int actualPage = 1;

    private ArrayList<Product> shopProducts;
    private int pagesCount = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        VBox.setMargin(mainVBox, new Insets(20));

        initSortByComboBox();
        initCategoryComboBox();

        loadShopProducts();

        nextPageButton.setOnAction(e->{
            nextPage(shopProducts);
        });

        previousPageButton.setOnAction(e->{
            previousPage(shopProducts);
        });


        searchButton.setOnAction(e->{
            shopProducts = new ProductModel().getProducts(categoryComboBox.getValue(), sortBy, searchTextField.getText());
            actualPage = 1;

            if(shopProducts.isEmpty())
                actualPage = 0;

            updateShopProducts(shopProducts, 1);
            pagesCount = (int)Math.ceil(shopProducts.size() / (double)PER_PAGE);
            updatePageLabel(pagesCount);

        });

    }

    private void initSortByComboBox(){
        sortByComboBox.setItems(
                FXCollections.observableArrayList(
                        new String("Najlacnejšie"),
                        new String("Najdrahšie"),
                        new String("Abecedne zostupne"),
                        new String("Abecedne vzostupne")
                )
        );

        sortByComboBox.getSelectionModel().selectFirst();

        sortByComboBox.valueProperty().addListener((obs, oldValue, newValue)->{
            sortBy = getSortBy();
            loadShopProducts();
        });

    }

    private void initCategoryComboBox(){
        categoryComboBox.setItems(
                FXCollections.observableArrayList(
                        new ProductModel().getAllCategories(StoreModel.getInstance().getAllStoreChains().get(0)))
        );

        categoryComboBox.getSelectionModel().selectFirst();

        categoryComboBox.valueProperty().addListener((obs, oldValue, newValue)->{
            loadShopProducts();
        });
    }

    private void loadShopProducts(){
        shopProducts = new ProductModel().getProducts(categoryComboBox.getValue(), sortBy, searchTextField.getText());

        if(shopProducts.isEmpty())
            actualPage = 0;

        updateShopProducts(shopProducts, 1);

        pagesCount = (int)Math.ceil(shopProducts.size() / (double)PER_PAGE);
        updatePageLabel(pagesCount);
    }

    private void nextPage(ArrayList<Product> shopProducts){
//        loading.startLoading();
        if(shopProducts.isEmpty()){
            actualPage = 0;
            return;
        }
        int pagesCount = (int)Math.ceil(shopProducts.size() / (double)PER_PAGE);
        actualPage += 1;
        if(actualPage % (pagesCount+1) == 0) {
            actualPage = 1;
        }

        updateShopProducts(shopProducts, actualPage);
        updatePageLabel(pagesCount);
//        loading.stopLoading();
    }

    private void previousPage(ArrayList<Product> shopProducts){
        if(shopProducts.isEmpty()){
            actualPage = 0;
            return;
        }
        int pagesCount = (int)Math.ceil(shopProducts.size() / (double)PER_PAGE);
        actualPage -= 1;
        if(actualPage == 0)
            actualPage = pagesCount;
        updateShopProducts(shopProducts, actualPage);

        updatePageLabel(pagesCount);

    }

    private void updatePageLabel(int pagesCount) {
        String pagesString = actualPage + "/" + pagesCount;
        pagesLabel.setText(pagesString);
        paginationHBox.setVisible(pagesCount > 1);
    }

    public void updateShopProducts(ArrayList<Product> shopProducts, int page){
        shopProductsVBox.getChildren().clear();
        HBox newRow = createNewShopProductsRow();
        int size = Math.min(shopProducts.size(), ((page) * PER_PAGE));
        for(int i=(page-1)*PER_PAGE; i <size; i++){
            boolean isNewRow = i%3==0;
            if(isNewRow)
                newRow = createNewShopProductsRow();
            newRow.getChildren().add(createNewShopProduct(shopProducts.get(i), isNewRow));
        }
    }


    /**
     * Method which creates new shop product as VBox
     * @param isFirst if isFirst is true, we won't set margin on the left side
     * @return VBox which contains product given as parameter
     */
    private VBox createNewShopProduct(Product product, boolean isFirst){
        VBox newProduct = new VBox();
        newProduct.setMaxWidth(230);

        ImageView newImageView = new ImageView();
        newImageView.setFitHeight(220);
        newImageView.setFitWidth(220);


        try{
            Image productImage = new Image(product.getImageUrl());
            if (productImage.isError()) {
                throw new ImageUrlException("Couldn't find image with given url");
            }
            newImageView.setImage(productImage);
        }
        catch(ImageUrlException e){
            getLogger().error(e.getMessage());
        }

        Label productPriceLabel = new Label (String.format("%.2f", product.getPrice()) + " €");
        productPriceLabel.getStyleClass().add("price-label");

        Label productNameLabel = new Label(product.getName());
        productNameLabel.getStyleClass().add("product-name");
        productNameLabel.setMinHeight(80);
        productNameLabel.setWrapText(true);
//        productNameLabel.setWrapText(true);

        // add imageView with labels to VBox
        newProduct.getChildren().add(newImageView);
        newProduct.getChildren().add(productPriceLabel);
        newProduct.getChildren().add(productNameLabel);

        VBox.setMargin(productNameLabel, new Insets(0,0,0,10));
        VBox.setMargin(productPriceLabel, new Insets(0,0,0,10));

        if(!isFirst) {
            HBox.setMargin(newProduct, new Insets(0, 0, 0, 30));
        }
        newProduct.getStyleClass().add("shop-product");
        newProduct.setCursor(Cursor.HAND);

        newProduct.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(event.getButton().equals(MouseButton.PRIMARY)){
                    ColorAdjust colorAdjust = new ColorAdjust();
                    colorAdjust.setBrightness(-0.4);
                    colorAdjust.setContrast(-0.2);
                    ViewManager.getInstance().getBorderPane().setEffect(colorAdjust);

                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ProductDetail.fxml"));
                        Parent productDetail = (Parent) loader.load();
                        ProductDetailController controller = loader.getController();
                        controller.setShopProduct(product);

                        Stage popupStage = new Stage(StageStyle.TRANSPARENT);
                        double positionX = ViewManager.getInstance().getPrimaryStage().getX() + 360;
                        double positionY = ViewManager.getInstance().getPrimaryStage().getY() + 250;
                        popupStage.setX(positionX);
                        popupStage.setY(positionY);

                        popupStage.initOwner(ViewManager.getInstance().getPrimaryStage());
                        popupStage.initModality(Modality.APPLICATION_MODAL);
                        popupStage.setScene(new Scene(productDetail, Color.TRANSPARENT));

                        ViewManager.getInstance().setPopupStage(popupStage);

                        controller.getCloseButton().setOnAction(e ->{
                            ViewManager.getInstance().getBorderPane().setEffect(null);
                            popupStage.hide();
                        });

                        popupStage.show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return newProduct;
    }


    /**
     * Method which creates new row for shop products, where we can add shop products
     * @return empty HBox
     */
    private HBox createNewShopProductsRow(){
        HBox shopProductsRow = new HBox();
        shopProductsRow.setPrefHeight(270);
        VBox.setMargin(shopProductsRow, new Insets(15, 0, 15, 0));

//      add new row to shop products VBox
        shopProductsVBox.getChildren().add(shopProductsRow);

        return shopProductsRow;
    }

    public void openShoppingCart(){
        ViewManager.getInstance().setCenterView("shoppingCart");
    }

    private ProductSortBy getSortBy(){
        switch(sortByComboBox.getValue()){
            case "Najlacnejšie":
                return ProductSortBy.PRICE_ASC;
            case "Najdrahšie":
                return ProductSortBy.PRICE_DESC;
            case "Abecedne zostupne":
                return ProductSortBy.NAME_DESC;
            case "Abecedne vzostupne":
                return ProductSortBy.NAME_ASC;
            default:
                return ProductSortBy.PRICE_ASC;
        }
    }
}
