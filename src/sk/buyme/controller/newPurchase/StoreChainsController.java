package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.store.StoreChain;
import sk.buyme.model.store.StoreModel;
import sk.buyme.model.user.Application;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StoreChainsController extends Loggable implements Initializable {

    @FXML
    private VBox storeChainsVBox;

    @FXML
    private Button cancelButton;

    @FXML
    private Button confirmationButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        confirmationButton.setDisable(true);
        for(StoreChain storeChain: StoreModel.getInstance().getAllStoreChains()){
            addStoreChain(storeChain);
        }
        getLogger().info("Store chains successfully loaded");

    }

    public void addStoreChain(StoreChain newStoreChain){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/StoreChainItem.fxml"));
            Parent storeChainItem = loader.load();
            StoreChainItemController controller = loader.getController();
            controller.setStoreChain(newStoreChain);

            VBox.setMargin(storeChainItem, new Insets(0, 40, 20, 40));
            storeChainsVBox.getChildren().add(storeChainItem);

            storeChainItem.setOnMouseClicked(e->{
                Application.getInstance().setSelectedStoreChain(newStoreChain);
                setSelectedStoreChain(storeChainItem);
            });

        }
        catch (IOException  e){
            getLogger().error("Couldn't load fxml file");

        }
    }

    private void setSelectedStoreChain(Parent selectedStoreChain) {
        Parent oldSelection = Application.getInstance().getSelectedStoreChainParent();
        if(oldSelection!=null){
            oldSelection.getStyleClass().clear();
            oldSelection.getStyleClass().add("shop-item");
        }

        confirmationButton.setDisable(false);

        Application.getInstance().setSelectedStoreChainParent(selectedStoreChain);

        selectedStoreChain.getStyleClass().clear();
        selectedStoreChain.getStyleClass().add("shop-item-selected");
    }

    public void removeStoreChainSelection(){
        confirmationButton.setDisable(true);
        Parent oldSelection = Application.getInstance().getSelectedStoreChainParent();
        if(oldSelection!=null){
            oldSelection.getStyleClass().clear();
            oldSelection.getStyleClass().add("shop-item");
        }

        Application.getInstance().setSelectedStoreChain(null);
    }

    public void confirmation(){
        ViewManager.getInstance().setCenterView("shopLocation");

    }

    public void cancel(){
        ViewManager.getInstance().setCenterView("myPurchases");

    }
}
