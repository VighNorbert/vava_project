package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.ImageUrlException;
import sk.buyme.model.products.Product;
import sk.buyme.model.products.ShoppingCart;
import sk.buyme.model.products.ShoppingCartItem;

import java.net.URL;
import java.util.ResourceBundle;

public class ProductDetailController extends Loggable implements Initializable {

    private Product product;

    @FXML
    private ImageView productImageView;
    @FXML
    private Label productNameLabel;
    @FXML
    private Label productPriceLabel;
    @FXML
    private Button closeButton;
    @FXML
    private Button addToShoppingCartButton;
    @FXML
    private Spinner<Integer> numberSpinner;




    @Override
    public void initialize(URL location, ResourceBundle resources) {
        numberSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 9999));
    }

    public void setShopProduct(Product product){
        this.product = product;
        productNameLabel.setText(product.getName());
        productPriceLabel.setText(String.format("%.2f",product.getPrice()) + " €/ks");

        try{
            Image productImage = new Image(product.getImageUrl());
            if (productImage.isError()) {
                throw new ImageUrlException("Couldn't find image with given url");
            }
            productImageView.setImage(productImage);
        }
        catch(ImageUrlException e){
            getLogger().error(e.getMessage());
        }


    }

    public Button getCloseButton(){
        return closeButton;
    }

    public void addToShoppingCart(){
        int quantity = numberSpinner.getValue();

        ShoppingCartItem newItem = new ShoppingCartItem(product, quantity);
        ShoppingCart.getInstance().addShoppingCartItem(newItem);

        ShoppingCartController controller = (ShoppingCartController)
                ViewManager.getInstance().getController("shoppingCart");

        controller.addShoppingCartItem(newItem);

        getLogger().info("Product " + product.getName() + " was added to shopping cart");

        ViewManager.getInstance().getBorderPane().setEffect(null);
        ViewManager.getInstance().getPopupStage().hide();

    }

}
