package sk.buyme.controller.newPurchase;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.IncorrectlyFilledData;
import sk.buyme.exceptions.RegistrationException;
import sk.buyme.model.address.AddressModel;
import sk.buyme.model.address.City;
import sk.buyme.model.address.District;
import sk.buyme.model.address.Region;
import sk.buyme.model.products.ShoppingCart;
import sk.buyme.model.store.Store;
import sk.buyme.model.store.StoreModel;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.User;
import sk.buyme.model.user.UserModel;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ShopLocationController extends Loggable implements Initializable {
    @FXML
    private ComboBox<Region> regionComboBox;

    @FXML
    private ComboBox<District> districtComboBox;

    @FXML
    private ComboBox<Store> shopComboBox;

    @FXML
    private Button nextButton;


    ObservableList<Region> regionObservableList = FXCollections.observableArrayList();
    ObservableList<District> districtObservableList = FXCollections.observableArrayList();
    ObservableList<Store> shopObservableList = FXCollections.observableArrayList();

    private final AddressModel addressModel = new AddressModel();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nextButton.setDisable(true);
        ArrayList<Region> regions = addressModel.getAllRegions();
        regionObservableList.addAll(regions);
        regionComboBox.setItems(regionObservableList);
        districtComboBox.setDisable(true);
        shopComboBox.setDisable(true);
    }

    public void changeOfComboRegion(){
        if (regionComboBox.getValue() != null) {
            districtComboBox.setDisable(false);
            ArrayList<District> districts = addressModel.getDistricts(regionComboBox.getValue());
            districtObservableList.clear();
            shopObservableList.clear();
            districtObservableList.addAll(districts);
            districtComboBox.setItems(districtObservableList);
            shopComboBox.getItems().clear();
            shopComboBox.setDisable(true);
            nextButton.setDisable(true);
        }
    }

    public void changeOfComboDistrict(){
        if (districtComboBox.getValue() != null) {
            shopComboBox.setDisable(false);
            shopComboBox.getItems().clear();
            shopObservableList.clear();
            shopObservableList.addAll(StoreModel.getInstance().getStores(
                    Application.getInstance().getSelectedStoreChain(),
                    districtComboBox.getValue()
            ));
            shopComboBox.setItems(shopObservableList);
            nextButton.setDisable(true);
        }
    }

    public void storeSelectionComplete() {
        String filled = checkPlacesValidation(regionComboBox, districtComboBox);
        if (filled.equals("")) {
            if(Application.getInstance().getSelectedStore() != shopComboBox.getValue())
                ShoppingCart.getInstance().clearShoppingCart();
            Application.getInstance().setSelectedStore(shopComboBox.getValue());
            ViewManager.getInstance().setCenterView("shopProducts");
        }
        else   try {
            throw new IncorrectlyFilledData(filled);
        } catch (IncorrectlyFilledData e) {
            getLogger().error("Incorrectly filled data");
            Window.display("Error", filled, WindowType.ALERT_WINDOW);
        }
    }

    public String checkPlacesValidation(ComboBox<Region> comboBoxRegion, ComboBox<District> comboBoxDistrict){
        if (comboBoxRegion.getValue() == null)
            return "Región nevyplnený";
        if (comboBoxDistrict.getValue() == null)
            return "Distrikt nevyplnený";
        return "";
    }

    @FXML
    void changeOfComboShop() {
        if(!shopObservableList.isEmpty() && shopComboBox.getValue()!=null)
            nextButton.setDisable(false);
    }

    public void resetShopSelection(){
        regionComboBox.getSelectionModel().clearSelection();
        districtComboBox.getSelectionModel().clearSelection();
        shopComboBox.getSelectionModel().clearSelection();
        districtComboBox.setDisable(true);
        shopComboBox.setDisable(true);
        Application.getInstance().setSelectedStore(null);
    }
}
