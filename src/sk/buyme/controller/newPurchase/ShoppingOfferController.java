package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.controller.mainWindow.OverviewPurchasesController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.products.ShoppingCartItem;
import sk.buyme.model.purchase.Purchase;
import sk.buyme.model.purchase.PurchaseModel;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.AvatarViewer;
import sk.buyme.model.user.User;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ShoppingOfferController extends Loggable implements Initializable {

    @FXML
    private VBox shoppingListVBox;
    @FXML
    private Label totalPriceLabel;
    @FXML
    private Label shoppingPriceLabel;
    @FXML
    private Label deliveryPriceLabel;
    @FXML
    private ImageView avatarImageView;
    @FXML
    private Label customerNameLabel;
    @FXML
    private Label customerAddressLabel;

    private Purchase actualPurchase;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void updateShoppingOffer(Purchase purchase){
        shoppingListVBox.getChildren().clear();
        actualPurchase = purchase;
        customerNameLabel.setText(purchase.getCustomer().getName());
        customerAddressLabel.setText(purchase.getStreet() + ",\n " + purchase.getCity());
        User customer = purchase.getCustomer();
        AvatarViewer.showAvatar(customer, avatarImageView);
        ArrayList<ShoppingCartItem> shoppingListItems = purchase.getProducts();

        for (ShoppingCartItem listItem:
                shoppingListItems) {

            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShoppingListItem.fxml"));
                Parent shoppingListItem = loader.load();
                ShoppingListItemController controller = loader.getController();
                controller.setShoppingListItem(listItem);

                VBox.setMargin(shoppingListItem, new Insets(0, 0, 20, 0));

                shoppingListVBox.getChildren().add(shoppingListItem);
            } catch (IOException e) {
                getLogger().error("Couldn't load fxml file");
            }

            updatePrice(shoppingListItems);
        }
    }

    private void updatePrice(ArrayList<ShoppingCartItem> shoppingListItems) {
        double shoppingPrice = 0d;
        for (ShoppingCartItem shoppingCartItem:
                shoppingListItems) {
            shoppingPrice += shoppingCartItem.getPrice();
        }

        double deliveryPrice = Math.max(shoppingPrice * 0.15, 2.00);
        double totalPrice = deliveryPrice + shoppingPrice;
        deliveryPriceLabel.setText(String.format("%.2f", deliveryPrice) + " €");
        shoppingPriceLabel.setText(String.format("%.2f", shoppingPrice) + " €");
        totalPriceLabel.setText(String.format("%.2f", totalPrice) + " €");
    }

    public void back(){
        ViewManager.getInstance().setCenterView("overviewPurchases");
    }

    public void acceptPurchase(){
        if(Window.display("Prijatie nákupu", "Naozaj chcete prijať tento nákup?", WindowType.CONFIRM_WINDOW)) {
            actualPurchase.setBuyer(Application.getInstance().getLoggedInUser());
            new PurchaseModel().assignBuyer(actualPurchase);
            OverviewPurchasesController controller = (OverviewPurchasesController)ViewManager.getInstance().getController("overviewPurchases");
            controller.setData();
            ViewManager.getInstance().setCenterView("overviewPurchases");
        }
    }

}
