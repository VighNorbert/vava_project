package sk.buyme.controller.newPurchase;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.ImageUrlException;
import sk.buyme.model.products.ShoppingCartItem;
import sk.buyme.model.purchase.Purchase;
import sk.buyme.model.purchase.PurchaseModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ShoppingCheckListItemController extends Loggable implements Initializable {

    private ShoppingCartItem actualItem = null;
    private Purchase myPurchase = null;

    @FXML
    private ImageView productImageView;

    @FXML
    private Label productNameLabel;

    @FXML
    private Label productPriceLabel;

    @FXML
    private Label totalPriceLabel;

    @FXML
    private Label quantityLabel;

    @FXML
    private CheckBox purchasedCheckBox;

    @FXML
    private CheckBox notAvailableCheckBox;

    @FXML
    private HBox itemHBox;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setShoppingListItem(ShoppingCartItem newItem, Purchase purchase) {
        actualItem = newItem;
        myPurchase = purchase;
        productNameLabel.setText(actualItem.getProduct().getName());
        productPriceLabel.setText(String.format("%.2f", actualItem.getProduct().getPrice()) + " €/ks");
        totalPriceLabel.setText(String.format("%.2f", actualItem.getPrice()) + " €");
        quantityLabel.setText(Integer.toString(newItem.getQuantity()) + " ks");

        try{
            Image productImage = new Image(newItem.getProduct().getImageUrl());
            if (productImage.isError()) {
                throw new ImageUrlException("Couldn't find image with given url");
            }
            productImageView.setImage(productImage);
        }
        catch(ImageUrlException e){
            getLogger().error(e.getMessage());
        }
    }

    @FXML
    void purchasedChange() {
        if(purchasedCheckBox.isSelected()) {
            itemHBox.getStyleClass().clear();
            itemHBox.getStyleClass().add("center-content");
            itemHBox.getStyleClass().add("shopping-check-list-purchased");
            notAvailableCheckBox.setSelected(false);
            actualItem.setAvailable(true);
            ShoppingCheckListController controller = (ShoppingCheckListController) ViewManager.getInstance().getController("shoppingCheckList");
            controller.removeUnavailableItem(actualItem);
            controller.updatePrice();
        }
        else {
            itemHBox.getStyleClass().clear();
            itemHBox.getStyleClass().add("center-content");
            actualItem.setAvailable(true);
        }
        new PurchaseModel().saveProductsState(myPurchase);
    }

    @FXML
    void unavailableChanged() {
        if(notAvailableCheckBox.isSelected()) {
            itemHBox.getStyleClass().clear();
            itemHBox.getStyleClass().add("center-content");
            itemHBox.getStyleClass().add("shopping-check-list-unavailable");
            actualItem.setAvailable(false);
            purchasedCheckBox.setSelected(false);
            ShoppingCheckListController controller = (ShoppingCheckListController) ViewManager.getInstance().getController("shoppingCheckList");
            controller.addUnavailableItem(actualItem);
            controller.updatePrice();
        }
        else {
            itemHBox.getStyleClass().clear();
            itemHBox.getStyleClass().add("center-content");
            actualItem.setAvailable(true);

            ShoppingCheckListController controller = (ShoppingCheckListController) ViewManager.getInstance().getController("shoppingCheckList");
            controller.removeUnavailableItem(actualItem);
            controller.updatePrice();
        }
        new PurchaseModel().saveProductsState(myPurchase);
    }

    public void setUnavailable(){
        itemHBox.getStyleClass().clear();
        itemHBox.getStyleClass().add("center-content");
        itemHBox.getStyleClass().add("shopping-check-list-unavailable");
        actualItem.setAvailable(false);
        purchasedCheckBox.setSelected(false);
        notAvailableCheckBox.setSelected(true);
        ShoppingCheckListController controller = (ShoppingCheckListController) ViewManager.getInstance().getController("shoppingCheckList");
        controller.addUnavailableItem(actualItem);
        controller.updatePrice();
    }

    public void setBought(){
        itemHBox.getStyleClass().clear();
        itemHBox.getStyleClass().add("center-content");
        itemHBox.getStyleClass().add("shopping-check-list-purchased");
        notAvailableCheckBox.setSelected(false);
        purchasedCheckBox.setSelected(true);
        actualItem.setAvailable(true);
        ShoppingCheckListController controller = (ShoppingCheckListController) ViewManager.getInstance().getController("shoppingCheckList");
        controller.removeUnavailableItem(actualItem);
        controller.updatePrice();
    }

    public CheckBox getPurchasedCheckBox() {
        return purchasedCheckBox;
    }

    public CheckBox getNotAvailableCheckBox() {
        return notAvailableCheckBox;
    }

}
