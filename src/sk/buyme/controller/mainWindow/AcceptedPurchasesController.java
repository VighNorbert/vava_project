package sk.buyme.controller.mainWindow;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.controller.myProfile.PurchaseController;
import sk.buyme.model.products.Product;
import sk.buyme.model.purchase.Purchase;
import sk.buyme.model.purchase.PurchaseModel;
import sk.buyme.model.purchase.PurchaseType;
import sk.buyme.model.store.Store;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.User;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class AcceptedPurchasesController extends Loggable implements Initializable {

    @FXML
    private VBox vboxPurchasesAccepted;

    @FXML
    private HBox paginationHBox;

    @FXML
    private Button nextPageButton;

    @FXML
    private Button previousPageButton;

    @FXML
    private Label pagesLabel;

    @FXML
    private VBox vboxPurchasesBought;

    @FXML
    private HBox paginationHBox1;

    @FXML
    private Button previousPageButton1;

    @FXML
    private Label pagesLabel1;

    @FXML
    private Button nextPageButton1;

    private final int PER_PAGE = 10;
    private int actualPage = 1;
    private int actualBoughtPage = 1;
    private int pagesCount = 0;
    private int pagesBoughtCount = 0;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        nextPageButton.setOnAction(e->{
            nextPage();
        });

        previousPageButton.setOnAction(e->{
            previousPage();
        });

        nextPageButton1.setOnAction(e->{
            nextBoughtPage();
        });

        previousPageButton1.setOnAction(e->{
            previousBoughtPage();
        });
  }

    public Parent createPurchase(Purchase purchase) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/Purchase.fxml"));
            Parent purchases = (Parent) loader.load();
            PurchaseController purchaseController = loader.getController();
//            new PurchaseModel().loadProducts(purchase);
            purchaseController.setPurchases(purchase, PurchaseType.SHOPPING_CHECKLIST);
            VBox.setMargin(purchases, new Insets(0, 0, 20, 0));
            return purchases;
        } catch (IOException e) {
            getLogger().error("Purchase didn't load correctly");
        }
        return null;
    }

    public void setData(){
        vboxPurchasesAccepted.getChildren().clear();
        vboxPurchasesBought.getChildren().clear();
        ArrayList<Purchase> purchaseArrayList = new PurchaseModel().getByBuyer(Application.getInstance().getLoggedInUser(), true);
        ArrayList<Purchase> purchaseBoughtArrayList = new PurchaseModel().getByBuyer(Application.getInstance().getLoggedInUser(), false);

        if(actualPage==0 && !purchaseArrayList.isEmpty()) {
            actualPage = 1;
        }

        int size = Math.min(purchaseArrayList.size(), ((actualPage) * PER_PAGE));


        pagesCount = (int)Math.ceil(purchaseArrayList.size() / (double)PER_PAGE);
        updatePageLabel(pagesCount);

        paginationHBox.setVisible(pagesCount > 1);

        if(purchaseArrayList.isEmpty()){
            actualPage = 0;
            updatePageLabel(pagesCount);
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/EmptyPurchases.fxml"));
                Parent emptyCartImage = loader.load();
                vboxPurchasesAccepted.getChildren().add(emptyCartImage);
            } catch (IOException e) {
                getLogger().error("Empty cart image wasn't loaded", e);
            }
        }

        if(actualPage!=0) {
            for (int i = (actualPage - 1) * PER_PAGE; i < size; i++) {
                vboxPurchasesAccepted.getChildren().add(createPurchase(purchaseArrayList.get(i)));
            }
        }

        if(actualBoughtPage==0 && !purchaseBoughtArrayList.isEmpty()) {
            actualBoughtPage=1;
        }

        int sizeBought = Math.min(purchaseBoughtArrayList.size(), ((actualBoughtPage) * PER_PAGE));

        pagesBoughtCount = (int)Math.ceil(purchaseBoughtArrayList.size() / (double)PER_PAGE);
        updateBoughtPageLabel(pagesBoughtCount);

        paginationHBox1.setVisible(pagesBoughtCount > 1);

        if(purchaseBoughtArrayList.isEmpty()){
            actualBoughtPage = 0;
            updateBoughtPageLabel(pagesBoughtCount);
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/EmptyPurchases.fxml"));
                Parent emptyCartImage = loader.load();
                vboxPurchasesBought.getChildren().add(emptyCartImage);
            } catch (IOException e) {
                getLogger().error("Empty cart image wasn't loaded", e);
            }
        }

        if(actualBoughtPage!=0) {
            for (int i = (actualBoughtPage - 1) * PER_PAGE; i < sizeBought; i++) {
                vboxPurchasesBought.getChildren().add(createPurchase(purchaseBoughtArrayList.get(i)));
            }
        }
    }

    private void nextPage(){
        actualPage += 1;
        if(actualPage % (pagesCount+1) == 0)
            actualPage = 1;
        setData();
    }

    private void previousPage(){
        actualPage -= 1;
        if(actualPage == 0)
            actualPage = pagesCount;
        setData();
    }

    private void updatePageLabel(int pagesCount) {
        String pagesString = actualPage + "/" + pagesCount;
        pagesLabel.setText(pagesString);
    }


    private void nextBoughtPage(){
        actualBoughtPage += 1;
        if(actualBoughtPage % (pagesBoughtCount+1) == 0)
            actualBoughtPage = 1;
        setData();
    }

    private void previousBoughtPage(){
        actualBoughtPage -= 1;
        if(actualBoughtPage == 0)
            actualBoughtPage = pagesBoughtCount;
        setData();
    }

    private void updateBoughtPageLabel(int pagesCount) {
        String pagesBoughtString = actualBoughtPage + "/" + pagesBoughtCount;
        pagesLabel1.setText(pagesBoughtString);
    }

}

