package sk.buyme.controller.mainWindow;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class AppInfoController implements Initializable {
    @FXML
    private Label aboutLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        aboutLabel.setText("\nVerzia - 1.0\n\n\n" +
                "Táto aplikácia sa snaží pomôcť ľudom ktorý buď nemajú radi nakupovanie \nalebo nemôžu ísť nakupovať" +
                "\n\n\n\n Aplikácia bola vytvorená tímom:      Norbert Vígh" +
                "\n\n\n                                                                     Juraj Zalabai" +
                "\n\n\n                                                                     Adrián Kabáč" +
                "\n\n\n\nS pomocou:       https://icons.getbootstrap.com/" +
                "\n                              https://undraw.co/search\n" +
                "                              https://getavataaars.com/");
    }
}
