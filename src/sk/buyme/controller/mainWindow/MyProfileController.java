package sk.buyme.controller.mainWindow;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import sk.buyme.Loggable;
import sk.buyme.controller.newPurchase.ShopLocationController;
import sk.buyme.controller.newPurchase.StoreChainsController;
import sk.buyme.controller.surrouding.SideMenuController;
import sk.buyme.controller.surrouding.SideMenuPreLoginController;
import sk.buyme.model.products.ShoppingCart;
import sk.buyme.model.user.Application;
import sk.buyme.controller.myProfile.PersonalInformationController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.user.AvatarViewer;
import sk.buyme.model.user.User;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ResourceBundle;

public class MyProfileController extends Loggable implements Initializable {
    @FXML
    private Label labelName;

    @FXML
    private Label usernameLabel;

    @FXML
    private Label emailLabel;

    @FXML
    private Label addressLabel;

    @FXML
    private Pane avatarPane;

    @FXML
    private Label cityLabel;

    @FXML
    public ImageView avatarImageView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setUser() {
        User u = Application.getInstance().getLoggedInUser();
        usernameLabel.setText(u.getUsername());
        emailLabel.setText(u.getEmail());
        addressLabel.setText(u.getStreet());
        labelName.setText(u.getName());
        cityLabel.setText(u.getCity().toString());
        AvatarViewer.showAvatar(u, avatarImageView);
    }

    public void personalInformationScene(){
        PersonalInformationController controller = (PersonalInformationController) ViewManager.getInstance().getController("personalInfo");
        controller.setData(Application.getInstance().getLoggedInUser());
        ViewManager.getInstance().setCenterView("personalInfo");
    }

    public void logout(){
        if (Window.display("Odhlásenie", "Naozaj sa chcete odhlásiť ?", WindowType.CONFIRM_WINDOW)) {
            Application.getInstance().setLoggedInUser(null);
            ViewManager.getInstance().setCenterView("login");
            ViewManager.getInstance().setSideMenu("sideMenuPreLogin");

//      reset shop selection
            ShopLocationController shopLocationController = (ShopLocationController) ViewManager.getInstance().getController("shopLocation");
            shopLocationController.resetShopSelection();

//      reset store chains selection
            StoreChainsController storeChainsController = (StoreChainsController) ViewManager.getInstance().getController("storeChains");
            storeChainsController.removeStoreChainSelection();

//      reset shopping cart items
            ShoppingCart.getInstance().clearShoppingCart();

            SideMenuPreLoginController sideMenuController = (SideMenuPreLoginController) ViewManager.getInstance().getController("sideMenuPreLogin");
            sideMenuController.resetButtonAfterLogout();
        }

    }

    public void feedback(){
        ViewManager.getInstance().setCenterView("feedback");
    }

}
