package sk.buyme.controller.mainWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.controller.myProfile.PurchaseController;
import sk.buyme.model.address.City;
import sk.buyme.model.products.ProductSortBy;
import sk.buyme.model.purchase.Purchase;
import sk.buyme.model.purchase.PurchaseModel;
import sk.buyme.model.purchase.PurchaseSortBy;
import sk.buyme.model.purchase.PurchaseType;
import sk.buyme.model.store.Store;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.User;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class OverviewPurchasesController extends Loggable implements Initializable {

    @FXML
    private VBox vboxPurchases;

    @FXML
    private HBox paginationHBox;

    @FXML
    private Button nextPageButton;

    @FXML
    ComboBox<String> comboBoxSort;

    @FXML
    private Button previousPageButton;

    @FXML
    private Label pagesLabel;

    private final int PER_PAGE = 10;
    private int actualPage = 1;
    private int pagesCount = 0;

    ObservableList<String> sortObservableList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nextPageButton.setOnAction(e->{
            nextPage();
        });

        previousPageButton.setOnAction(e->{
            previousPage();
        });
        sortObservableList.add("Najnovšie");
        sortObservableList.add("Najstaršie");
        sortObservableList.add("Najdrahšie");
        sortObservableList.add("Najlacnejšie");
        comboBoxSort.setItems(sortObservableList);
        comboBoxSort.setValue(sortObservableList.get(0));
    }

    private PurchaseSortBy getSortBy(){
        switch(comboBoxSort.getValue()){
            case "Najlacnejšie":
                return PurchaseSortBy.PRICE_ASC;
            case "Najdrahšie":
                return PurchaseSortBy.PRICE_DESC;
            case "Najstaršie":
                return PurchaseSortBy.DATE_ASC;
            case "Najnovšie":
                return PurchaseSortBy.DATE_DESC;
            default:
                return PurchaseSortBy.PRICE_ASC;
        }
    }

    public Parent createPurchase(Purchase purchase) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/Purchase.fxml"));
            Parent purchases = (Parent) loader.load();
            PurchaseController purchaseController = loader.getController();
            purchaseController.setPurchases(purchase, PurchaseType.SHOPPING_OFFER);
            VBox.setMargin(purchases, new Insets(0, 0, 20, 0));
            return purchases;
        } catch (IOException e) {
            getLogger().error("Purchase didn't load correctly");
        }
        return null;
    }

    public void setData(){
        vboxPurchases.getChildren().clear();
        ArrayList<Purchase> purchaseArrayList = new PurchaseModel().getUnassigned(getSortBy(), Application.getInstance().getLoggedInUser());

        if(actualPage==0 && !purchaseArrayList.isEmpty()) {
            actualPage=1;
        }

        int size = Math.min(purchaseArrayList.size(), ((actualPage) * PER_PAGE));

        pagesCount = (int)Math.ceil(purchaseArrayList.size() / (double)PER_PAGE);
        updatePageLabel(pagesCount);

        paginationHBox.setVisible(pagesCount > 1);

        if(purchaseArrayList.isEmpty()){
            actualPage = 0;
            updatePageLabel(pagesCount);
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/EmptyPurchases.fxml"));
                Parent emptyCartImage = loader.load();
                vboxPurchases.getChildren().add(emptyCartImage);
            } catch (IOException e) {
                getLogger().error("Empty cart image wasn't loaded", e);
            }

        }

        if(actualPage>0) {
            for (int i = (actualPage - 1) * PER_PAGE; i < size; i++) {
                vboxPurchases.getChildren().add(createPurchase(purchaseArrayList.get(i)));
            }
        }
    }

    private void nextPage(){
        actualPage += 1;
        if(actualPage % (pagesCount+1) == 0)
            actualPage = 1;
        setData();
    }

    private void previousPage(){
        actualPage -= 1;
        if(actualPage == 0)
            actualPage = pagesCount;
        setData();
    }

    private void updatePageLabel(int pagesCount) {
        String pagesString = actualPage + "/" + pagesCount;
        pagesLabel.setText(pagesString);
    }
}
