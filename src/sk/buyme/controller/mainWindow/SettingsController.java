package sk.buyme.controller.mainWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import sk.buyme.controller.myProfile.PurchaseController;
import sk.buyme.controller.surrouding.SideMenuController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.address.Region;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {

    @FXML
    private ComboBox<String> textFieldLanguage;

    @FXML
    private Label settingsLabel;

    @FXML
    private Label languagesLabel;

    @FXML
    private Button settingsInsideButton;

    private final Locale local_en = new Locale("en", "US");
    private final Locale local_sk = new Locale("sk", "SK");
    private final Locale local_de = new Locale("de", "DE");
    private final ResourceBundle resourceBundle = ResourceBundle.getBundle("sk.buyme/resources/bundles/resourceBundle_sk", local_sk);
    private final ResourceBundle resourceBundleDE = ResourceBundle.getBundle("sk.buyme/resources/bundles/resourceBundle_de", local_en);
    private final ResourceBundle resourceBundleEN = ResourceBundle.getBundle("sk.buyme/resources/bundles/resourceBundle_en", local_de);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> languageObservable = FXCollections.observableArrayList();
        languageObservable.add("Slovenčina");
        languageObservable.add("English");
        languageObservable.add("Deutsch");
        textFieldLanguage.setItems(languageObservable);
        textFieldLanguage.setValue("Slovenčina");
    }

    public void setLanguage(){
        SideMenuController controller = (SideMenuController) ViewManager.getInstance().getController("sideMenu");
        if (textFieldLanguage.getValue().equals("Slovenčina")) {
            controller.setData(resourceBundle);
            setLanguageInside(resourceBundle);
        }
        else if (textFieldLanguage.getValue().equals("English")){
            controller.setData(resourceBundleEN);
            setLanguageInside(resourceBundleEN);
        }
        else if (textFieldLanguage.getValue().equals("Deutsch")){
            controller.setData(resourceBundleDE);
            setLanguageInside(resourceBundleDE);
        }
    }

    public void setLanguageInside(ResourceBundle resourceBundle){
        settingsLabel.setText(resourceBundle.getString("settingsLabel.text"));
        languagesLabel.setText(resourceBundle.getString("languagesLabel.text"));
        settingsInsideButton.setText(resourceBundle.getString("settingsInsideButton.text"));

    }


}
