package sk.buyme.controller.mainWindow;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sk.buyme.Loggable;
import sk.buyme.controller.myProfile.PurchaseController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.model.purchase.Purchase;
import sk.buyme.model.purchase.PurchaseModel;
import sk.buyme.model.purchase.PurchaseType;
import sk.buyme.model.store.Store;
import sk.buyme.model.user.Application;
import sk.buyme.model.user.User;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MyPurchasesController extends Loggable implements Initializable {

    @FXML
    public VBox vboxMyPurchases;

    @FXML
    private VBox VBoxActualPurchases;

    @FXML
    private HBox paginationHBox;

    @FXML
    private Button nextPageButton;

    @FXML
    private Button previousPageButton;

    @FXML
    private Label pagesLabel;

    private final int PER_PAGE = 10;
    private int actualPage = 1;
    private int pagesCount = 0;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        nextPageButton.setOnAction(e->{
            nextPage();
        });

        previousPageButton.setOnAction(e->{
            previousPage();
        });

//        Purchase purchase = new Purchase(new User("jc", "Jano cibula"), new Store("Tesco"), 32.12, 19.09);
//        vboxMyPurchases.getChildren().add(createPurchase(purchase));
//        vboxMyPurchases.getChildren().add(createPurchase(purchase));
//        vboxMyPurchases.getChildren().add(createPurchase(purchase));
//        vboxMyPurchases.getChildren().add(createPurchase(purchase));
//        vboxMyPurchases.getChildren().add(createPurchase(purchase));
//        vboxMyPurchases.getChildren().add(createPurchase(purchase));
//        vboxMyPurchases.getChildren().add(createPurchase(purchase));
    }

    public Parent createPurchase(Purchase purchase) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/Purchase.fxml"));
            Parent purchases = (Parent) loader.load();
            PurchaseController purchaseController = loader.getController();
            if(purchase.getBuyer()==null)
                purchaseController.setPurchases(purchase, PurchaseType.SHOPPING_LIST);
            else
                purchaseController.setPurchases(purchase, PurchaseType.SHOPPING_LIST_ACCEPTED);

            VBox.setMargin(purchases, new Insets(0, 0, 20, 0));
            return purchases;
        } catch (IOException e) {
            getLogger().error("Purchase didn't load correctly");
        }
        return null;
    }

    public void setData(){
        vboxMyPurchases.getChildren().clear();
        VBoxActualPurchases.getChildren().clear();

        ArrayList<Purchase> purchaseHistoryArrayList = new PurchaseModel().getByCustomer(Application.getInstance().getLoggedInUser(), false);
        ArrayList<Purchase> actualPurchaseArrayList = new PurchaseModel().getByCustomer(Application.getInstance().getLoggedInUser(), true);

        if(actualPurchaseArrayList.isEmpty()) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/EmptyPurchases.fxml"));
                Parent purchases = (Parent) loader.load();
                VBoxActualPurchases.getChildren().add(purchases);
            } catch (IOException e) {
                getLogger().error("Empty cart image wasn't loaded", e);
            }
        }

        for (Purchase purchase: actualPurchaseArrayList) {
            VBoxActualPurchases.getChildren().add(createPurchase(purchase));
        }

        if(actualPage==0 && !purchaseHistoryArrayList.isEmpty()) {
            actualPage=1;
        }

        int size = Math.min(purchaseHistoryArrayList.size(), ((actualPage) * PER_PAGE));


        pagesCount = (int)Math.ceil(purchaseHistoryArrayList.size() / (double)PER_PAGE);
        updatePageLabel(pagesCount);

        paginationHBox.setVisible(pagesCount > 1);

        if(purchaseHistoryArrayList.isEmpty()){
            actualPage = 0;
            updatePageLabel(pagesCount);
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/EmptyPurchases.fxml"));
                Parent purchases = (Parent) loader.load();
                vboxMyPurchases.getChildren().add(purchases);
            } catch (IOException e) {
                getLogger().error("Empty cart image wasn't loaded", e);
            }
            return;
        }

        for (int i=(actualPage-1)*PER_PAGE; i <size; i++) {
            vboxMyPurchases.getChildren().add(createPurchase(purchaseHistoryArrayList.get(i)));
        }
    }

    public void addPurchases(){
        ViewManager.getInstance().setCenterView("storeChains");
    }

    private void nextPage(){
        actualPage += 1;
        if(actualPage % (pagesCount+1) == 0)
            actualPage = 1;
        setData();
    }

    private void previousPage(){
        actualPage -= 1;
        if(actualPage == 0)
            actualPage = pagesCount;
        setData();
    }

    private void updatePageLabel(int pagesCount) {
        String pagesString = actualPage + "/" + pagesCount;
        pagesLabel.setText(pagesString);
    }

}
