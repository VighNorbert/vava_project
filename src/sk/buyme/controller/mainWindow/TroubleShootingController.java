package sk.buyme.controller.mainWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import sk.buyme.Loggable;
import sk.buyme.model.MailSender;
import sk.buyme.model.address.City;
import sk.buyme.model.address.Region;
import sk.buyme.model.user.Application;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class TroubleShootingController extends Loggable implements Initializable {

    @FXML
    private ComboBox<String> troubleCombo;

    @FXML
    private TextArea feedbackTextArea;

    ObservableList<String> problemsObservableList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        ArrayList<District> districts = addressModel.getDistricts(regions.get(0));
//        ArrayList<City> cities = new AddressModel().getCities(districts.get(0));
        problemsObservableList.add("Môj nákup");
        problemsObservableList.add("Vybavovanie nákupu");
        problemsObservableList.add("Problémy s aplikáciou");
        problemsObservableList.add("Účet");
        problemsObservableList.add("Iné");
        troubleCombo.setItems(problemsObservableList);
        troubleCombo.setValue("Môj nákup");
    }

    public void troubleShoot() {
        try {
            MailSender.sendMail("buymeappconfirmation@gmail.com", "Trouble shooting - "+troubleCombo.getValue(), "Meno a priezvisko: " + Application.getInstance().getLoggedInUser().getName() + "<br/><br/>Prihlasovacie meno: " + Application.getInstance().getLoggedInUser().getUsername() + "<br/><br/>" +  feedbackTextArea.getText());
            Window.display("Podpora", "Ďakujeme za kontakovanie podpory.\nK Vášmu problému sa dostaneme hneď ako sa bude dať.", WindowType.ALERT_WINDOW);

        } catch (Exception e) {
            getLogger().error(e.getMessage());
        }
    }
}
