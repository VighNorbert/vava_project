package sk.buyme.controller.surrouding;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sk.buyme.Loggable;
import sk.buyme.controller.mainWindow.OverviewPurchasesController;
import sk.buyme.exceptions.UserNotConfirmedException;
import sk.buyme.model.MailSender;
import sk.buyme.model.user.Application;
import sk.buyme.exceptions.AuthenticationException;
import sk.buyme.model.user.User;
import sk.buyme.model.user.UserModel;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController extends Loggable implements Initializable {

    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usernameField.clear();
        passwordField.clear();
    }

    public void registerView(){
        ViewManager.getInstance().setCenterView("registrationInfo");
    }

    public void loginButton(){
        User user = null;
        try {
            try {
                user = new UserModel().authenticate(usernameField.getText(), passwordField.getText());
                Application.getInstance().setLoggedInUser(user);
            } catch (UserNotConfirmedException e) {
                if (MailSender.check("pop.gmail.com", "pop3", "buymeappconfirmation@gmail.com", "confirmation9", e.getUser().getEmail())){
                    new UserModel().confirmUser(e.getUser());
                    Application.getInstance().setLoggedInUser(e.getUser());
                }
                else throw e;
            }
            OverviewPurchasesController controller = (OverviewPurchasesController) ViewManager.getInstance().getController("overviewPurchases");
            controller.setData();
            ViewManager.getInstance().setCenterView("overviewPurchases");

            SideMenuController sideMenuController = (SideMenuController) ViewManager.getInstance().getController("sideMenu");
            sideMenuController.resetButtonAfterLogin();

            ViewManager.getInstance().setSideMenu("sideMenu");
        } catch (AuthenticationException e) {
            getLogger().error("Incorrect data for login");
            Window.display("Správa", "Nesprávne údaje", WindowType.ALERT_WINDOW);
        }
        catch(UserNotConfirmedException e){
            getLogger().error("Unconfirmed account");
            Window.display("Nepotvrdený email", "Nemáte potvrdené údaje", WindowType.ALERT_WINDOW);
        }
    }
}
