package sk.buyme.controller.surrouding;

import sk.buyme.Loggable;
import sk.buyme.controller.surrouding.RegistrationPlaceController;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.exceptions.DifferentPasswordsException;
import sk.buyme.exceptions.IncorrectlyFilledData;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class RegistrationInfoController extends Loggable implements Initializable {


    @FXML
    private TextField nameField;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private PasswordField repeatPasswordField;

    @FXML
    private Button loginButton;

    @FXML
    private Button registerButton;

    @FXML
    private TextField emailField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nameField.clear();
        usernameField.clear();
        passwordField.clear();
        repeatPasswordField.clear();
        emailField.clear();
    }

    public void registration(){
        String filled = regexRegistrationCheck(nameField, usernameField, emailField, passwordField, repeatPasswordField);
            if (filled.equals("")) {
                try {
                    if (checkPasswordsSame(passwordField, repeatPasswordField)) {
                        ViewManager.getInstance().setCenterView("avatarCreation");
////                        ViewManager.getInstance().setCenterView("registrationPlace");
                        AvatarCreationController.setUserInfo(usernameField.getText(), nameField.getText(), emailField.getText(), passwordField.getText());
                    } else
                        throw new DifferentPasswordsException("Odlišné heslá");
                    } catch(DifferentPasswordsException e){
                        getLogger().error("Passwords do not match");
                        Window.display("Nezhodujúce sa heslá", "Vaše heslá sa nezhodujú", WindowType.ALERT_WINDOW);
                    }
                }
            else
                try {
                    throw new IncorrectlyFilledData(filled);
                } catch (IncorrectlyFilledData e) {
                    getLogger().error("Incorretly filled data");
                    Window.display("Error", filled, WindowType.ALERT_WINDOW);
                }
    }

    public String regexRegistrationCheck(TextField textFieldMeno, TextField usernameField, TextField emailField, PasswordField passwordField, PasswordField repeatPasswordField){
        if (!Pattern.matches("^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$",usernameField.getText())){
            return "Prihlasovanie meno nesprávne vyplnené\nMinimálne 4 znaky, bez špeciálnych znakov";}
        if (!Pattern.matches("\\p{L}+[ ]\\p{L}+",textFieldMeno.getText())){
            return "Meno a priezvisko nesprávne vyplnené";}
        if (!Pattern.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$",emailField.getText())){
            return "Email nesprávne vyplnený";}
        if (!passwordValidation(passwordField.getText())){
            return "Heslo nesprávne vyplnené(minimum 6 znakov, minimálne 1 veľké písmeno,\n minimálne 1 malé písmeno a 1 číslo)";}
        if (!passwordValidation(repeatPasswordField.getText())){
            return "Zopakované heslo nesprávne vyplnené(minimum 6 znakov, minimálne 1 veľké písmeno,\n minimálne 1 malé písmeno a 1 číslo)";}
        return "";
    }

    public static boolean passwordValidation(String password){
        if (Pattern.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}$",password)) {
            return true;
        }
        return false;
    }

    public static boolean checkPasswordsSame(PasswordField textFieldFirstPassowrd, PasswordField textFieldSecondPassword){
        if (textFieldFirstPassowrd.getText().equals(textFieldSecondPassword.getText())) {
            return true;
        }
        else {
            return false;
        }
    }
}
