package sk.buyme.controller.surrouding;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
import sk.buyme.Loggable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class BorderPaneWindowController extends Loggable implements Initializable {

    @FXML
    private BorderPane borderPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            ViewManager.getInstance().setBorderPane(borderPane);

            FXMLLoader loadingScreen = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/LoadingScreen.fxml"));
            FXMLLoader sideMenuPreLogin = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/SideMenuPreLogin.fxml"));
            FXMLLoader topMenu = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/TopMenu.fxml"));
            ViewManager.getInstance().addView("loadingScreen", loadingScreen);
            ViewManager.getInstance().addView("sideMenuPreLogin", sideMenuPreLogin);
            ViewManager.getInstance().addView("topMenu", topMenu);
            ViewManager.getInstance().setCenterView("loadingScreen");
            ViewManager.getInstance().setTop("topMenu");
            ViewManager.getInstance().setSideMenu("sideMenuPreLogin");

            getLogger().debug("nacitana prva cast");

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void loadingScreen(){
        try {
            FXMLLoader sideMenu = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/SideMenu.fxml"));
            FXMLLoader login = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/Login.fxml"));
            FXMLLoader registerInfo = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/RegistrationInfo.fxml"));
            FXMLLoader registerPlace = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/RegistrationPlace.fxml"));
            FXMLLoader shops = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/StoreChains.fxml"));
            FXMLLoader shopProducts = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShopProducts.fxml"));
            FXMLLoader shoppingCart = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShoppingCart.fxml"));
            FXMLLoader overviewPurchases = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/OverviewPurchases.fxml"));
            FXMLLoader myProfile = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/MyProfile.fxml"));
            FXMLLoader myPurchases = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/MyPurchases.fxml"));
            FXMLLoader acceptedPurchases = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/AcceptedPurchases.fxml"));
            FXMLLoader settings = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/Settings.fxml"));
            FXMLLoader appInfo = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/AppInfo.fxml"));
            FXMLLoader shoppingList = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShoppingList.fxml"));
            FXMLLoader shoppingListAccepted = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShoppingListAccepted.fxml"));
            FXMLLoader personalInfo = new FXMLLoader(getClass().getResource("/sk/buyme/view/myProfile/PersonalInformation.fxml"));
            FXMLLoader changePassword = new FXMLLoader(getClass().getResource("/sk/buyme/view/myProfile/ChangePassword.fxml"));
            FXMLLoader changeAddress = new FXMLLoader(getClass().getResource("/sk/buyme/view/myProfile/ChangeAddress.fxml"));
            FXMLLoader feedback = new FXMLLoader(getClass().getResource("/sk/buyme/view/myProfile/FeedBack.fxml"));
            FXMLLoader storeChains = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/StoreChains.fxml"));
            FXMLLoader shopLocation = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShopLocation.fxml"));
            FXMLLoader destinationLocation = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/DestinationLocation.fxml"));
            FXMLLoader shoppingOffer = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShoppingOffer.fxml"));
            FXMLLoader shoppingCheckList = new FXMLLoader(getClass().getResource("/sk/buyme/view/newPurchases/ShoppingCheckList.fxml"));
            FXMLLoader avatarCreation = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/AvatarCreation.fxml"));
            FXMLLoader troubleShooting = new FXMLLoader(getClass().getResource("/sk/buyme/view/mainWindow/TroubleShooting.fxml"));
            FXMLLoader changeAvatar = new FXMLLoader(getClass().getResource("/sk/buyme/view/myProfile/ChangeAvatar.fxml"));

            ViewManager.getInstance().addView("login", login);
            ViewManager.getInstance().addView("registrationInfo", registerInfo);
            ViewManager.getInstance().addView("registrationPlace", registerPlace);
            ViewManager.getInstance().addView("shops", shops);
            ViewManager.getInstance().addView("shopProducts", shopProducts);
            ViewManager.getInstance().addView("sideMenu", sideMenu);
            ViewManager.getInstance().addView("overviewPurchases", overviewPurchases);
            ViewManager.getInstance().addView("shoppingCart", shoppingCart);
            ViewManager.getInstance().addView("myProfile", myProfile);
            ViewManager.getInstance().addView("myPurchases", myPurchases);
            ViewManager.getInstance().addView("acceptedPurchases", acceptedPurchases);
            ViewManager.getInstance().addView("settings", settings);
            ViewManager.getInstance().addView("appInfo", appInfo);
            ViewManager.getInstance().addView("shoppingList", shoppingList);
            ViewManager.getInstance().addView("shoppingListAccepted", shoppingListAccepted);
            ViewManager.getInstance().addView("personalInfo", personalInfo);
            ViewManager.getInstance().addView("changePassword", changePassword);
            ViewManager.getInstance().addView("changeAddress", changeAddress);
            ViewManager.getInstance().addView("feedback", feedback);
            ViewManager.getInstance().addView("storeChains", storeChains);
            ViewManager.getInstance().addView("shopLocation", shopLocation);
            ViewManager.getInstance().addView("destinationLocation", destinationLocation);
            ViewManager.getInstance().addView("shoppingOffer", shoppingOffer);
            ViewManager.getInstance().addView("shoppingCheckList", shoppingCheckList);
            ViewManager.getInstance().addView("avatarCreation", avatarCreation);
            ViewManager.getInstance().addView("troubleShooting", troubleShooting);
            ViewManager.getInstance().addView("changeAvatar", changeAvatar);

            ViewManager.getInstance().setCenterView("login");
            ViewManager.getInstance().setTop("topMenu");
            ViewManager.getInstance().setSideMenu("sideMenuPreLogin");
        }
        catch (IOException e){
            getLogger().error("Error in initializing pane");
        }
    }
}
