package sk.buyme.controller.surrouding;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import sk.buyme.model.address.AddressModel;
import sk.buyme.model.address.City;
import sk.buyme.model.address.District;
import sk.buyme.model.address.Region;
import sk.buyme.model.user.Avatar;
import sk.buyme.model.user.AvatarPossibilities;
import sk.buyme.model.user.AvatarViewer;
import sk.buyme.model.user.User;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class AvatarCreationController implements Initializable {

    @FXML
    private ComboBox<String> topTextField;

    @FXML
    private ComboBox<String> accessoriesTextField;

    @FXML
    private ComboBox<String> hairColorTextField;

    @FXML
    private ComboBox<String> facialHairTypeTextField;

    @FXML
    private ComboBox<String> facialHairColorTextField;

    @FXML
    private ComboBox<String> clotheTypeTextField;

    @FXML
    private ComboBox<String> clotheColorTextField;

    @FXML
    private ComboBox<String> eyeTypeTextField;

    @FXML
    private ComboBox<String> eyebrowTypeTextField;

    @FXML
    private ComboBox<String> mouthTypeTextField;

    @FXML
    private ComboBox<String> skinColorTextField;

    @FXML
    private ImageView img;

    private static User user = new User();

    ObservableList<String> topObservableList = FXCollections.observableArrayList();
    ObservableList<String> accessoriesObservableList = FXCollections.observableArrayList();
    ObservableList<String> hairColorObservableList = FXCollections.observableArrayList();
    ObservableList<String> facialHairTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> clotheTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> clotheColorObservableList = FXCollections.observableArrayList();
    ObservableList<String> eyeTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> eyebrowTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> mouthTypeObservableList = FXCollections.observableArrayList();
    ObservableList<String> facialHairColorObservableList = FXCollections.observableArrayList();
    ObservableList<String> skinColorObservableList = FXCollections.observableArrayList();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        topObservableList.addAll(AvatarPossibilities.getTopType());
        topTextField.setItems(topObservableList);

        accessoriesObservableList.addAll(AvatarPossibilities.getAccessoriesType());
        accessoriesTextField.setItems(accessoriesObservableList);

        hairColorObservableList.addAll(AvatarPossibilities.getHairColor());
        hairColorTextField.setItems(hairColorObservableList);

        facialHairTypeObservableList.addAll(AvatarPossibilities.getFacialHairType());
        facialHairTypeTextField.setItems(facialHairTypeObservableList);

        clotheTypeObservableList.addAll(AvatarPossibilities.getClotheType());
        clotheTypeTextField.setItems(clotheTypeObservableList);

        clotheColorObservableList.addAll(AvatarPossibilities.getClotheColor());
        clotheColorTextField.setItems(clotheColorObservableList);

        eyeTypeObservableList.addAll(AvatarPossibilities.getEyeType());
        eyeTypeTextField.setItems(eyeTypeObservableList);

        eyebrowTypeObservableList.addAll(AvatarPossibilities.getEyebrowType());
        eyebrowTypeTextField.setItems(eyebrowTypeObservableList);

        mouthTypeObservableList.addAll(AvatarPossibilities.getMouthType());
        mouthTypeTextField.setItems(mouthTypeObservableList);

        facialHairColorObservableList.addAll(AvatarPossibilities.getFacialHairColor());
        facialHairColorTextField.setItems(facialHairColorObservableList);

        skinColorObservableList.addAll(AvatarPossibilities.getSkinType());
        skinColorTextField.setItems(skinColorObservableList);

        updateAvatar();

//
    }

    public void updateAvatar(){
        Avatar avatar = new Avatar(topTextField.getValue(), accessoriesTextField.getValue(), hairColorTextField.getValue(),
                facialHairTypeTextField.getValue(), facialHairColorTextField.getValue(), clotheTypeTextField.getValue(),
                clotheColorTextField.getValue(), eyeTypeTextField.getValue(), eyebrowTypeTextField.getValue(),
                mouthTypeTextField.getValue(), skinColorTextField.getValue());
        user.setAvatar(avatar);
        AvatarViewer.showAvatar(avatar, img, null);

    }

    public static void setUserInfo(String username, String name, String email, String password){
        user.setUsername(username);
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
    }

    public void setNextScreen(){
        ViewManager.getInstance().setCenterView("registrationPlace");
        RegistrationPlaceController.setUserInfo(user.getUsername(),  user.getName(), user.getEmail(), user.getPassword(), user.getAvatar());
    }

    public void goBack(){
        ViewManager.getInstance().setCenterView("registrationInfo");
    }

}
