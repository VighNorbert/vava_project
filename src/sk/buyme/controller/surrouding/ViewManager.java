package sk.buyme.controller.surrouding;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sk.buyme.Loggable;

import java.io.IOException;
import java.util.HashMap;


public class ViewManager extends Loggable {
    private static ViewManager instance = null;

    private BorderPane borderPane;

    private HashMap<String, Parent> scenes = new HashMap<>();

    private HashMap<Parent, FXMLLoader> sceneLoaders = new HashMap<>();

    private Stage primaryStage;

    private Stage popupStage;

    private ViewManager() {

    }

    public static ViewManager getInstance(){
        if(instance == null)
            instance = new ViewManager();
        return instance;
    }

    public BorderPane getBorderPane() {
        return borderPane;
    }

    public void setBorderPane(BorderPane borderPane) {
        this.borderPane = borderPane;
    }

    public boolean addView(String name, FXMLLoader loader)throws IOException {
        if (scenes.containsKey(name)){
            getLogger().error("this name is already used");
            return false;
        }
        Parent parent = loader.load();
        scenes.put(name, parent);
        sceneLoaders.put(parent, loader);
        return true;
    }

    public Object getController(String name){
        FXMLLoader loader = sceneLoaders.get(getView(name));
        return loader.getController();
    }

    public Parent getView(String name){
        try{
            Parent scene = scenes.get(name);
            if(scene == null)
                throw new NullPointerException();
            return scene;
        }
        catch (NullPointerException e){
            getLogger().error("View with given name does not exist");
            return null;
        }
    }

    public void setCenterView(String name){
        if(borderPane != null){
            Parent center = getView(name);
            borderPane.setCenter(center);
        }
        else{
            getLogger().error("Border pane wasn't set yet");
        }
    }

    public void setSideMenu(String name){
        if(borderPane != null){
            Parent sideMenu = getView(name);
            borderPane.setLeft(sideMenu);
        }
        else{
            getLogger().error("Border pane wasn't set yet");
        }
    }

    public void setTop(String name){
        if(borderPane != null){
            Parent top = getView(name);
            borderPane.setTop(top);
        }
        else{
            System.out.println("Border pane wasn't set yet");
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public Stage getPopupStage() {
        return popupStage;
    }

    public void setPopupStage(Stage popupStage) {
        this.popupStage = popupStage;
    }
}
