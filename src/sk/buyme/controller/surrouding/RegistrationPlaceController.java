package sk.buyme.controller.surrouding;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.omg.CORBA.MARSHAL;
import sk.buyme.Loggable;
import sk.buyme.exceptions.IncorrectlyFilledData;
import sk.buyme.exceptions.RegistrationException;
import sk.buyme.model.MailSender;
import sk.buyme.model.address.AddressModel;
import sk.buyme.model.address.City;
import sk.buyme.model.address.District;
import sk.buyme.model.address.Region;
import sk.buyme.model.user.Avatar;
import sk.buyme.model.user.User;
import sk.buyme.model.user.UserModel;
import sk.buyme.model.window.Window;
import sk.buyme.model.window.WindowType;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class RegistrationPlaceController extends Loggable implements Initializable {

    @FXML
    private ComboBox<Region> regionComboBox;

    @FXML
    private ComboBox<District> districtComboBox;

    @FXML
    private ComboBox<City> cityComboBox;

    @FXML
    private TextField addressTextField;

    ObservableList<Region> regionObservableList = FXCollections.observableArrayList();
    ObservableList<District> districtObservableList = FXCollections.observableArrayList();
    ObservableList<City> cityObservableList = FXCollections.observableArrayList();

    private final AddressModel addressModel = new AddressModel();

    private static User user = new User();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ArrayList<Region> regions = addressModel.getAllRegions();
//        ArrayList<District> districts = addressModel.getDistricts(regions.get(0));
//        ArrayList<City> cities = new AddressModel().getCities(districts.get(0));
        regionObservableList.addAll(regions);
        regionComboBox.setItems(regionObservableList);
//
//        districtObservableList.addAll(districts);
//        districtComboBox.setItems(districtObservableList);
//
//        cityObservableList.addAll(cities);
//        cityComboBox.setItems(cityObservableList);
//
        districtComboBox.setDisable(true);
        cityComboBox.setDisable(true);
        addressTextField.setDisable(true);
    }

    public void changeOfComboRegion(){
        districtComboBox.setDisable(false);
        ArrayList<District> districts = addressModel.getDistricts(regionComboBox.getValue());
        districtObservableList.clear();
        districtObservableList.addAll(districts);
        districtComboBox.setItems(districtObservableList);
        cityComboBox.setDisable(true);
        addressTextField.setDisable(true);
        addressTextField.clear();
        cityObservableList.clear();
        cityComboBox.setItems(cityObservableList);
    }

    public void changeOfComboDistrict(){
        if (districtComboBox.getValue() != null) {
            cityComboBox.setDisable(false);
            ArrayList<City> cities = addressModel.getCities(districtComboBox.getValue());
            cityObservableList.clear();
            cityObservableList.addAll(cities);
            cityComboBox.setItems(cityObservableList);
            addressTextField.setDisable(true);
            addressTextField.clear();
        }
    }

    public void changeOfComboCity() {
        if (!cityComboBox.isDisabled()) {
            addressTextField.setDisable(false);
            addressTextField.clear();
        }
    }

    public void registrationComplete() throws Exception {
        String filled = checkPlacesValidation(regionComboBox, districtComboBox, cityComboBox, addressTextField);
        if (filled.equals("")) {
            //Mail.sendMail("xkabac@stuba.sk", "Confirmation BuyMe", "Please confirm this");
            user.setCity(cityComboBox.getValue());
            user.setStreet(addressTextField.getText());
            sentUserToDatabase(user);
        }
        else   try {
            throw new IncorrectlyFilledData(filled);
        } catch (IncorrectlyFilledData e) {
            getLogger().error("Incorretly filled data");
            Window.display("Error", filled, WindowType.ALERT_WINDOW);
        }
    }

    public String checkPlacesValidation(ComboBox<Region> comboBoxRegion, ComboBox<District> comboBoxDistrict, ComboBox<City> comboBoxCity, TextField textFieldAddress){
        if (comboBoxRegion.getValue() == null)
            return "Región nevyplnený";
        if (comboBoxDistrict.getValue() == null)
            return "Distrikt nevyplnený";
        if (comboBoxCity.getValue() == null)
            return "Mesto nevyplnené";
        if (textFieldAddress.getText().equals(""))
            return "Adresa a číslo domu nesprávne vyplnené";
        return "";
    }

    public static void setUserInfo(String username, String name, String email, String password, Avatar avatar){
        user.setUsername(username);
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        user.setAvatar(avatar);
    }

    public void goBack(){
        ViewManager.getInstance().setCenterView("avatarCreation");
    }

    public void sentUserToDatabase(User user){
        try {
            new UserModel().register(user);
            MailSender.sendMail(user.getEmail(), "Potvrdenie emailu", "Dobrý deň <br/><br/> Ďakujeme že ste sa u nás zaregistrovali. <br/> Prosím potvrdťe registráciu odpísaním ľubovoľnej správy na tento e-mail <br/><br/> Neváhajte nás kontaktovať v prípade otázok.<br/><br/><br/> Váš tím aplikácie BuyMe <br/>");
            Window.display("Registrácia", "Registračný email poslaný, prosím potvrďte ho odpovedaním naň", WindowType.ALERT_WINDOW);
            ViewManager.getInstance().setCenterView("login");
        } catch (RegistrationException e) {
            // TODO username or email exists
            getLogger().error("Username or email exists");
            Window.display("Chyba pri registrácií", "Meno alebo email je už použité iným používateľom", WindowType.ALERT_WINDOW);
            ViewManager.getInstance().setCenterView("registrationInfo");
        } catch (Exception e) {
            e.printStackTrace();
            getLogger().error("Email not sent");
        }
    }

}
