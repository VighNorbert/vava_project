package sk.buyme.controller.surrouding;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import sk.buyme.Loggable;

import java.net.URL;
import java.util.ResourceBundle;

public class SideMenuPreLoginController extends Loggable implements Initializable {

    private Button selectedButton;

    @FXML
    private Button loginButton;

    @FXML
    private Button registrationButton;

    @FXML
    private Button aboutButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resetButtonAfterLogout();
    }

    public void loginView(){
        ViewManager.getInstance().setCenterView("login");
        setSelectedButton(loginButton);
    }
    public void registrationView(){
        ViewManager.getInstance().setCenterView("registrationInfo");
        setSelectedButton(registrationButton);
    }
    public void appInfo(){
        ViewManager.getInstance().setCenterView("appInfo");
        setSelectedButton(aboutButton);
    }

    public void exit(){
        System.exit(0);
    }

    public void setSelectedButton(Button button){
        if (!button.equals(selectedButton)) {
            if(selectedButton!=null) {
                selectedButton.setStyle("");
            }
            selectedButton = button;
            button.setStyle("-fx-background-color: rgba(0,0,0,0.3)");
        }
    }

    public void resetButtonAfterLogout(){
        if (selectedButton!=null) {
            selectedButton.setStyle("");
        }
        selectedButton = loginButton;
        selectedButton.setStyle("-fx-background-color: rgba(0,0,0,0.3)");
    }
}
