package sk.buyme.controller.surrouding;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;

public class WindowController implements Initializable {

    @FXML
    private Button closeButton;
    @FXML
    private Label titleLabel;
    @FXML
    private Label messageLabel;
    @FXML
    private Button okButton;
    @FXML
    private HBox buttonHbox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setData(String title, String message){
        titleLabel.setText(title);
        messageLabel.setText(message);
    }

    public Button getCloseButton(){
        return closeButton;
    }

    public Button getOkButton(){
        return okButton;
    }

    public HBox getButtonHbox(){
        return buttonHbox;
    }


}
