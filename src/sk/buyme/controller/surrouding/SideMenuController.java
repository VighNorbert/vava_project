package sk.buyme.controller.surrouding;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import sk.buyme.controller.mainWindow.AcceptedPurchasesController;
import sk.buyme.controller.mainWindow.MyProfileController;
import sk.buyme.controller.mainWindow.MyPurchasesController;
import sk.buyme.controller.mainWindow.OverviewPurchasesController;
import sk.buyme.model.user.Application;

import java.net.URL;
import java.util.ResourceBundle;

public class SideMenuController implements Initializable {

    @FXML
    private Button overviewPurchasesButton;

    @FXML
    private Button profileButton;

    @FXML
    private Button shoppingButton;

    @FXML
    private Button acceptedShoppingButton;

    @FXML
    private Button settingsButton;

    @FXML
    private Button aboutButton;

    @FXML
    private Button exitButton;

    @FXML
    private Button troubleShootingButton;


    private Button selectedButton = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void overviewPurchases(){
        OverviewPurchasesController controller = (OverviewPurchasesController) ViewManager.getInstance().getController("overviewPurchases");
        controller.setData();
        ViewManager.getInstance().setCenterView("overviewPurchases");
        setSelectedButton(overviewPurchasesButton);
    }
    public void myProfile(){
        MyProfileController controller = (MyProfileController) ViewManager.getInstance().getController("myProfile");
        controller.setUser();
        ViewManager.getInstance().setCenterView("myProfile");
        setSelectedButton(profileButton);
    }
    public void myPurchases(){
        MyPurchasesController controller = (MyPurchasesController) ViewManager.getInstance().getController("myPurchases");
        controller.setData();
        ViewManager.getInstance().setCenterView("myPurchases");
        setSelectedButton(shoppingButton);
    }
    public void myAcceptedPurchases(){
        AcceptedPurchasesController controller = (AcceptedPurchasesController) ViewManager.getInstance().getController("acceptedPurchases");
        controller.setData();
        ViewManager.getInstance().setCenterView("acceptedPurchases");
        setSelectedButton(acceptedShoppingButton);
    }
    public void settings(){
        ViewManager.getInstance().setCenterView("settings");
        setSelectedButton(settingsButton);
    }
    public void appInfo(){
        ViewManager.getInstance().setCenterView("appInfo");
        setSelectedButton(aboutButton);
    }

    public void trobuleShooting(){
        ViewManager.getInstance().setCenterView("troubleShooting");
        setSelectedButton(troubleShootingButton);
    }

    public void exit(){
        System.exit(0);
    }

    public void setSelectedButton(Button button){
        if (!button.equals(selectedButton)) {
            if(selectedButton!=null) {
                selectedButton.setStyle("");
            }
            selectedButton = button;
            button.setStyle("-fx-background-color: rgba(0,0,0,0.3)");
        }
    }

    public void setData(ResourceBundle resourceBundle){
        overviewPurchasesButton.setText(resourceBundle.getString("overviewPurchasesButton.text"));
        shoppingButton.setText(resourceBundle.getString("shoppingButton.text"));
        acceptedShoppingButton.setText(resourceBundle.getString("acceptedShoppingButton.text"));
        profileButton.setText(resourceBundle.getString("profileButton.text"));
        settingsButton.setText(resourceBundle.getString("settingsButton.text"));
        aboutButton.setText(resourceBundle.getString("aboutButton.text"));
        exitButton.setText(resourceBundle.getString("exitButton.text"));
        troubleShootingButton.setText(resourceBundle.getString("troubleShootingButton.text"));
    }

    public void resetButtonAfterLogin(){
        if (selectedButton!=null) {
            selectedButton.setStyle("");
        }
        selectedButton = overviewPurchasesButton;
        selectedButton.setStyle("-fx-background-color: rgba(0,0,0,0.3)");
    }

}
