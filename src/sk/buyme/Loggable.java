package sk.buyme;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Loggable {

    private static final Level LOG_LEVEL = Level.DEBUG;

    public Logger getLogger() {
        Logger logger = Logger.getLogger(this.getClass().getName());
        logger.setLevel(LOG_LEVEL);
        return logger;
    }
}