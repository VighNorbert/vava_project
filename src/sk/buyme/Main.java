package sk.buyme;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sk.buyme.controller.surrouding.BorderPaneWindowController;
import sk.buyme.controller.surrouding.ViewManager;

public class Main extends Application {
    public static Parent borderPane;
    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader borderPaneLoader = new FXMLLoader(getClass().getResource("/sk/buyme/view/surrouding/BorderPaneWindow.fxml"));
        ViewManager.getInstance().setPrimaryStage(primaryStage);
        borderPane = borderPaneLoader.load();
        primaryStage.setTitle("Buy me");
        primaryStage.show();
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/sk/buyme/logo/logoForIcon.png")));
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(borderPane, 1185, 800));
        ((BorderPaneWindowController)borderPaneLoader.getController()).loadingScreen();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
