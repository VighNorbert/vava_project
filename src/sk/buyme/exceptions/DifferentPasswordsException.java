package sk.buyme.exceptions;

public class DifferentPasswordsException extends Exception{

    public DifferentPasswordsException(String message) {
        super(message);
    }
}
