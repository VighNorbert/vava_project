package sk.buyme.exceptions;

public class ImageUrlException extends Exception{
    public ImageUrlException(String message) {
        super(message);
    }
}
