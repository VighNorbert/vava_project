package sk.buyme.exceptions;

public class IncorrectlyFilledData extends Exception{

    public IncorrectlyFilledData(String message) {
        super(message);
    }
}
