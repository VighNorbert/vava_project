package sk.buyme.exceptions;

public class AuthenticationException extends Exception {
    public static String getError() {
        return "Could not authenticate user ";
    }
}
