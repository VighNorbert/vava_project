package sk.buyme.exceptions;

import sk.buyme.model.user.User;

public class UserNotConfirmedException extends Exception {
    private final User user;

    public UserNotConfirmedException(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
