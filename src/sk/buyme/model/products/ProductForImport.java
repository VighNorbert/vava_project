package sk.buyme.model.products;

public class ProductForImport extends Product {
    private final ProductCategory pc;

    public ProductForImport(String name, String imageUrl, double price, String unit, ProductCategory pc) {
        super(name, imageUrl, price, unit);
        this.pc = pc;
    }

    public ProductCategory getPc() {
        return pc;
    }
}
