package sk.buyme.model.products;

public enum ProductSortBy {
    PRICE_ASC,
    PRICE_DESC,
    NAME_ASC,
    NAME_DESC
}
