package sk.buyme.model.products;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductCategory {
    private int id;
    private final String name;
    private final String link;

    public ProductCategory(int id, String name, String link) {
        this.id = id;
        this.name = name;
        this.link = link;
    }

    public ProductCategory(String name, String link) {
        this.name = name;
        this.link = link;
    }

    public ProductCategory(ResultSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.name = rs.getString("name");
        this.link = rs.getString("url");
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString() {
        return name;
    }
}
