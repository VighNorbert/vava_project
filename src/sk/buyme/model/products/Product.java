package sk.buyme.model.products;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Product {
    private int id;
    private final String name;
    private final String imageUrl;
    private final double price;
    private final String unit;

    public Product(String name, String imageUrl, double price, String unit) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.price = price;
        this.unit = unit;
    }

    public Product(ResultSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.name = rs.getString("name");
        this.imageUrl = rs.getString("url");
        this.price = rs.getDouble("price");
        this.unit = rs.getString("unit");
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}
