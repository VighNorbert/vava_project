package sk.buyme.model.products;

import sk.buyme.Loggable;
import sk.buyme.controller.newPurchase.ShoppingCartController;
import sk.buyme.controller.surrouding.ViewManager;

import java.util.ArrayList;

public class ShoppingCart extends Loggable {
    private static ShoppingCart instance= null;

    private ArrayList<ShoppingCartItem> shoppingCartItems;

    public ShoppingCart() {
        shoppingCartItems = new ArrayList<>();
    }

    public static ShoppingCart getInstance() {
        if(instance == null)
            instance = new ShoppingCart();
        return instance;
    }

    public void addShoppingCartItem(ShoppingCartItem newItem){
        shoppingCartItems.add(newItem);
    }

    public ArrayList<ShoppingCartItem> getShoppingCartItems() {
        return shoppingCartItems;
    }

    public void removeShoppingCartItem(ShoppingCartItem removeItem) {
        if( shoppingCartItems.remove(removeItem)){
            getLogger().info("Product " + removeItem.getProduct().getName() + " was removed from shopping cart");
        }
        else{
            getLogger().error("Shopping cart item doesn't exist");
        }
    }

    public double getShoppingPrice(){
        double totalPrice = 0;
        for (ShoppingCartItem shoppingCartItem:
             shoppingCartItems) {
            totalPrice += shoppingCartItem.getPrice();
        }
        return totalPrice;
    }

    public double getDeliveryPrice(){
        return Math.max(getShoppingPrice()*0.15, 2.00);
    }

    public double getTotalPrice(){
        return getDeliveryPrice() + getShoppingPrice();

    }

    public void clearShoppingCart(){
        getLogger().info("Shopping cart items were removed");
        shoppingCartItems.clear();
        ShoppingCartController controller = (ShoppingCartController) ViewManager.getInstance().getController("shoppingCart");
        controller.clearShoppingCart();
    }
}
