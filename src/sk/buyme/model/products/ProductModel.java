package sk.buyme.model.products;

import sk.buyme.model.Model;
import sk.buyme.model.store.StoreChain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductModel extends Model {
    public ArrayList<ProductCategory> getAllCategories(StoreChain storeChain) {
        ArrayList<ProductCategory> categories = new ArrayList<>();
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT " +
                        "product_category.* " +
                    "from product_category " +
                    "WHERE storechain_id = ? " +
                    "ORDER BY d_update"
            );
            ps.setInt(1, storeChain.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductCategory pc = new ProductCategory(rs);
                categories.add(pc);
            }
            getLogger().info("Returned all product categories");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return categories;
    }

    public ArrayList<Product> getProducts(ProductCategory category, ProductSortBy sortBy, String search) {
        ArrayList<Product> products = new ArrayList<>();
        try (Connection conn = getConnection()) {
            String order = "";
            switch (sortBy) {
                case NAME_ASC:
                    order = "name ASC";
                    break;
                case NAME_DESC:
                    order = "name DESC";
                    break;
                case PRICE_ASC:
                    order = "price ASC";
                    break;
                case PRICE_DESC:
                    order = "price DESC";
                    break;
            }
            search = '%' + search + '%';
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT " +
                        "product.* " +
                    "from product " +
                    "WHERE product_category_id = ? AND product.name ILIKE ? " +
                    "ORDER BY " + order
            );
            ps.setInt(1, category.getId());
            ps.setString(2, search);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs);
                products.add(p);
            }
            getLogger().info("Returned all products in category " + category.getName());
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return products;
    }
}
