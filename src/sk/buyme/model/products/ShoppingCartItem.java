package sk.buyme.model.products;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ShoppingCartItem {
    private int id;
    private Product product;
    private int quantity;
    private Boolean available = null;

    public ShoppingCartItem(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public ShoppingCartItem(int id, Product product, int quantity) {
        this.id = id;
        this.product = product;
        this.quantity = quantity;
    }

    public ShoppingCartItem(ResultSet rs) throws SQLException {
        this.id = rs.getInt("pp_id");
        this.product = new Product(rs);
        this.quantity = rs.getInt("quantity");
        this.available = rs.getBoolean("available");
        if (rs.wasNull()) {
            this.available = null;
        }
    }

    public int getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice(){
        double totalPrice = getProduct().getPrice() * quantity;
        BigDecimal bd = new BigDecimal(totalPrice).setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Boolean isAvailable() {
        return available;
    }
}
