package sk.buyme.model.address;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Region extends AddressPart {
    public Region(int id, String name) {
        super(id, name);
    }

    public Region(ResultSet rs) throws SQLException {
        super(rs);
    }
}
