package sk.buyme.model.address;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Country extends AddressPart {
    public Country(int id, String name) {
        super(id, name);
    }

    public Country(ResultSet rs) throws SQLException {
        super(rs);
    }
}
