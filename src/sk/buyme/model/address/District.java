package sk.buyme.model.address;

import java.sql.ResultSet;
import java.sql.SQLException;

public class District extends AddressPart {
    public District(int id, String name) {
        super(id, name);
    }

    public District(ResultSet rs) throws SQLException {
        super(rs);
    }
}