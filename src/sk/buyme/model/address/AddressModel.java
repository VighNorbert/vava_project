package sk.buyme.model.address;

import sk.buyme.model.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddressModel extends Model {

    public ArrayList<Region> getAllRegions() {
        ArrayList<Region> regions = new ArrayList<>();
        try (Connection conn = getConnection()) {
            ResultSet rs = conn.prepareStatement("SELECT * from region ORDER BY name").executeQuery();
            while (rs.next()) {
                Region r = new Region(rs);
                regions.add(r);
            }
            getLogger().info("Returned list of regions");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return regions;
    }

    public ArrayList<District> getDistricts(Region region) {
        ArrayList<District> districts = new ArrayList<>();
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT * from district WHERE region_id = ? ORDER BY name");
            ps.setInt(1, region.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                District district = new District(rs);
                districts.add(district);
            }
            getLogger().info("Returned list of districts in region " + region.getName());
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return districts;
    }

    public ArrayList<City> getCities(District district) {
        ArrayList<City> cities = new ArrayList<>();
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT * from city WHERE district_id = ? ORDER BY name");
            ps.setInt(1, district.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                City city = new City(rs);
                cities.add(city);
            }
            getLogger().info("Returned list of cities in district " + district.getName());
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return cities;
    }


}
