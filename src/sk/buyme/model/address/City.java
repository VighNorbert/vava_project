package sk.buyme.model.address;

import java.sql.ResultSet;
import java.sql.SQLException;

public class City extends AddressPart {
    public City(int id, String name) {
        super(id, name);
    }

    public City(ResultSet rs) throws SQLException {
        super(rs);
    }
}
