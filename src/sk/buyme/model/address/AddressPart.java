package sk.buyme.model.address;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class AddressPart {
    private final int id;
    private final String name;

    public AddressPart(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public AddressPart(ResultSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.name = rs.getString("name");
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return this.getName();
    }
}
