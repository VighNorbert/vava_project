package sk.buyme.model;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import sk.buyme.model.products.ProductCategory;
import sk.buyme.model.products.ProductForImport;
import sk.buyme.model.products.ProductModel;
import sk.buyme.model.store.StoreChain;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class StoreScraper extends Model {
    private static final Logger logger = Logger.getLogger(StoreScraper.class.getName());

    private static ArrayList<ProductForImport> scrape(ProductCategory cat) throws IOException {
        logger.debug("Importing products from category " + cat.getName());
        ArrayList<ProductForImport> products = new ArrayList<>();
        Document doc = Jsoup.connect(cat.getLink()).userAgent("Mozilla/5.0").get();
        while (true) {
            Elements es = doc.getElementsByClass("product-list--list-item");
            for (Element el : es) {
                String name = el.select(".product-details--content").first().text();
                double price = Math.round(Float.parseFloat(el.select(".value").first().text().replace(',', '.')) * 100) / 100d;
                String img = el.select("img").first().attr("src");
                ProductForImport p = new ProductForImport(name, img, price, "ks", cat);
                products.add(p);
            }
            Element nextbutton = doc.select("a.pagination--button.prev-next").last();
            if (nextbutton.hasClass("disabled")) {
                break;
            }
            doc = Jsoup.connect("https://potravinydomov.itesco.sk" + nextbutton.attr("href")).userAgent("Mozilla/5.0").get();
        }
        return products;
    }

    public static void importProducts() throws IOException {
        logger.setLevel(Level.DEBUG);
        try (Connection conn = getConnection()) {
            ArrayList<ProductCategory> cats = new ProductModel().getAllCategories(new StoreChain(1, "TESCO", ""));
            for (ProductCategory cat: cats) {
                ArrayList<ProductForImport> products = scrape(cat);
                String sql = "INSERT INTO product (name, product_category_id, url, price, unit) VALUES ";
                for (ProductForImport p : products) {
                    sql = sql.concat("('"
                            + p.getName().replace("'", "''") + "', "
                            + p.getPc().getId() + ", '"
                            + p.getImageUrl() + "', "
                            + p.getPrice() + ", '"
                            + p.getUnit() + "'),\n");
                }
                sql = sql.substring(0, sql.length() - 2);
                conn.prepareStatement(sql).executeLargeUpdate();
            }
        } catch (SQLException ex) {
            logger.error("Database error", ex);
        }
    }
}
