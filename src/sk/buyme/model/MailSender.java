package sk.buyme.model;

import org.apache.log4j.Logger;
import sk.buyme.Loggable;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailSender extends Loggable {

    // INSPIRATION FROM THIS CODE
    //https://www.tutorialspoint.com/javamail_api/javamail_api_checking_emails.htm

    public static boolean check(String host, String storeType, String user,
                             String password, String email)
    {
        try {

            Properties properties = new Properties();

            properties.put("mail.pop3.host", host);
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);

            Store store = emailSession.getStore("pop3s");

            store.connect(host, user, password);

            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_WRITE);

            Message[] messages = emailFolder.getMessages();

            for (int i = 0, n = messages.length; i < n; i++) {
                Message message = messages[i];
                if (message.getFrom()[0].toString().contains(email)){
                    message.setFlag(Flags.Flag.DELETED, true);
                    emailFolder.close(false);
                    store.close();
                    return true;
                }
            }

            emailFolder.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    // INSPIRATION FROM THIS CODE
// https://www.tutorialspoint.com/java/java_sending_email.htm

    public static void sendMail(String recepient,String subject, String newMessage) throws Exception {
        Properties properties = new Properties();
        //properties.put("buymeappconfirmation@gmail.com", "confirmation9", true);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        String nameAccount = "buymeappconfirmation@gmail.com";
        String passwordAccount = "confirmation9";

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(nameAccount, passwordAccount);
            }
        });

        Message message = prepareMessageHtml(session, nameAccount, recepient, subject, newMessage);

        Transport.send(message);
    }


    private static Message prepareMessageHtml(Session session, String nameAccount, String recepient,String subject, String newMessage){
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(nameAccount));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
            message.setSubject(subject);
            String html = "<br/> <h2> <b> " +
                    ""+ newMessage+" </b> </h2> ";
            message.setContent(html, "text/html; charset=UTF-8");
            return message;
        }
        catch (Exception ex){
            Logger logger = new Loggable().getLogger();
            logger.error("Failed to send mail");
        }
        return null;
    }

    private static Message prepareMessage(Session session, String nameAccount, String recepient,String subject, String newMessage){
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(nameAccount));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
            message.setSubject(subject);
            message.setText(newMessage);
            return message;
        }
        catch (Exception ex){
            Logger logger = new Loggable().getLogger();
            logger.error("Failed to send mail");
        }
        return null;
    }
}
