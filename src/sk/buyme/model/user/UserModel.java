package sk.buyme.model.user;

import sk.buyme.exceptions.AuthenticationException;
import sk.buyme.exceptions.RegistrationException;
import sk.buyme.exceptions.UserNotConfirmedException;
import sk.buyme.model.Model;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;

public class UserModel extends Model {
    public ArrayList<User> getAll() {
        ArrayList<User> users = new ArrayList<>();
        try (Connection conn = getConnection()) {
            ResultSet rs = conn.prepareStatement(
                    "SELECT " +
                        "users.*, " +
                        "city.name as city_name, " +
                        "city.district_id, " +
                        "district.name as district_name, " +
                        "district.region_id, " +
                        "region.name as region_name, " +
                        "region.country_id, " +
                        "country.name as country_name " +
                    "from users " +
                    "JOIN city on city.id = users.city_id " +
                    "JOIN district on district.id = city.district_id " +
                    "JOIN region on region.id = district.region_id " +
                    "JOIN country on country.id = region.country_id"
            ).executeQuery();
            while (rs.next()) {
                User u = new User(rs);
                users.add(u);
            }
            getLogger().info("Returned all users");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return users;
    }

    public User authenticate(String username, String password) throws AuthenticationException, UserNotConfirmedException {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT " +
                        "users.*, " +
                        "city.name as city_name, " +
                        "city.district_id, " +
                        "district.name as district_name, " +
                        "district.region_id, " +
                        "region.name as region_name, " +
                        "region.country_id, " +
                        "country.name as country_name " +
                    "from users " +
                    "JOIN city on city.id = users.city_id " +
                    "JOIN district on district.id = city.district_id " +
                    "JOIN region on region.id = district.region_id " +
                    "JOIN country on country.id = region.country_id " +
                    "WHERE users.username = ? OR users.email = ?");
            ps.setString(1, username);
            ps.setString(2, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String pwd_salt = rs.getString("password_salt");
                byte[] salt = Base64.getDecoder().decode(pwd_salt);

                KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 391);
                SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
                byte[] hash = factory.generateSecret(spec).getEncoded();
                if (rs.getString("password").equals(Base64.getEncoder().encodeToString(hash))) {
                    User u = new User(rs);
                    if (rs.getDate("d_confirm") == null) {
                        getLogger().info("User " + username + " authenticated but email was not yet confirmed");
                        throw new UserNotConfirmedException(u);
                    }
                    getLogger().info("User " + username + " authenticated");
                    return u;
                }
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ignored) {
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        getLogger().error(AuthenticationException.getError() + username);
        throw new AuthenticationException();
    }

    public void confirmUser(User u) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "UPDATE users " +
                            "SET d_confirm = now() where id = ?"
            );
            ps.setInt(1, u.getId());
            ps.executeUpdate();
            getLogger().info("User " + u.getUsername() + " was confirmed");

        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }

    public void register(User u) throws RegistrationException {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM users where username = ? or email = ?");
            ps.setString(1, u.getUsername());
            ps.setString(2, u.getEmail());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                getLogger().error("User with same username or email already exists");
                throw new RegistrationException();
            }
            PasswordPair pp = getPasswordHashing(u);
            if (pp == null) throw new RegistrationException();
            ps = conn.prepareStatement(
                "INSERT INTO " +
                "users (username, email, password, password_salt, name, street, city_id, " +
                        "top_type, accessories_type, hair_color, facial_hair_type, facial_hair_color, " +
                        "clothe_type, clothe_color, eye_type, eyebrow_type, mouth_type, skin_color) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            );
            ps.setString(1, u.getUsername());
            ps.setString(2, u.getEmail());
            ps.setString(3, pp.getPassword_hash());
            ps.setString(4, pp.getPassword_salt());
            ps.setString(5, u.getName());
            ps.setString(6, u.getStreet());
            ps.setInt(7, u.getCity().getId());
            Avatar avatar = u.getAvatar();
            ps.setString(8, avatar.getTopType());
            ps.setString(9, avatar.getAccessoriesType());
            ps.setString(10, avatar.getHairColor());
            ps.setString(11, avatar.getFacialHairType());
            ps.setString(12, avatar.getFacialHairColor());
            ps.setString(13, avatar.getClotheType());
            ps.setString(14, avatar.getClotheColor());
            ps.setString(15, avatar.getEyeType());
            ps.setString(16, avatar.getEyebrowType());
            ps.setString(17, avatar.getMouthType());
            ps.setString(18, avatar.getSkinColor());
            ps.executeUpdate();
            getLogger().info("User " + u.getUsername() + " was successfully registered");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }

    private PasswordPair getPasswordHashing(User u) {
        try {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[24];
            random.nextBytes(salt);
            String salt_hash = Base64.getEncoder().encodeToString(salt);
            String pwd = u.getPassword();
            KeySpec spec = new PBEKeySpec(pwd.toCharArray(), salt, 65536, 391);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = factory.generateSecret(spec).getEncoded();
            String pwd_hash = Base64.getEncoder().encodeToString(hash);
            return new PasswordPair(pwd_hash, salt_hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ignored) {
        }
        return null;
    }

    public void save(User user, boolean passwordChanged, boolean avatarChanged){
        try (Connection conn = getConnection()) {
            if (passwordChanged) {
                PasswordPair pp = getPasswordHashing(user);
                if (pp == null) throw new SQLException();
                PreparedStatement ps = conn.prepareStatement(
                        "UPDATE users " +
                        "SET password = ?, password_salt = ?, d_update = now() " +
                        "WHERE id = ?"
                );
                ps.setString(1, pp.getPassword_hash());
                ps.setString(2, pp.getPassword_salt());
                ps.setInt(3, user.getId());

                ps.executeUpdate();
                getLogger().info("User " + user.getUsername() + " was updated");
            } else if (avatarChanged) {
                PreparedStatement ps = conn.prepareStatement(
                        "UPDATE users " +
                        "SET top_type = ?, accessories_type = ?, hair_color = ?, facial_hair_type = ?, facial_hair_color = ?, " +
                            "clothe_type = ?, clothe_color = ?, eye_type = ?, eyebrow_type = ?, mouth_type = ?, skin_color = ?, d_avatar_update = now() " +
                        "WHERE id = ?"
                );
                Avatar avatar = user.getAvatar();
                ps.setString(1, avatar.getTopType());
                ps.setString(2, avatar.getAccessoriesType());
                ps.setString(3, avatar.getHairColor());
                ps.setString(4, avatar.getFacialHairType());
                ps.setString(5, avatar.getFacialHairColor());
                ps.setString(6, avatar.getClotheType());
                ps.setString(7, avatar.getClotheColor());
                ps.setString(8, avatar.getEyeType());
                ps.setString(9, avatar.getEyebrowType());
                ps.setString(10, avatar.getMouthType());
                ps.setString(11, avatar.getSkinColor());
                ps.setInt(12, user.getId());

                ps.executeUpdate();

                user.setAvatarUpdate();

                getLogger().info("User " + user.getUsername() + " was updated");
            } else {
                PreparedStatement ps = conn.prepareStatement(
                        "UPDATE users " +
                        "SET name = ?, street = ?, city_id = ?, d_update = now() " +
                        "WHERE id = ?"
                );
                ps.setString(1, user.getName());
                ps.setString(2, user.getStreet());
                ps.setInt(3, user.getCity().getId());
                ps.setInt(4, user.getId());

                ps.executeUpdate();
                getLogger().info("User " + user.getUsername() + " was updated");
            }
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }
}
