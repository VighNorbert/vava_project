package sk.buyme.model.user;

import java.util.ArrayList;

public class AvatarPossibilities {

    public static ArrayList<String> getTopType() {
        ArrayList<String> topType = new ArrayList<>();
        topType.add("NoHair");
        topType.add("Eyepatch");
        topType.add("Hat");
        topType.add("Hijab");
        topType.add("Turban");
        topType.add("WinterHat1");
        topType.add("WinterHat2");
//        topType.add("WinterHat3");
//        topType.add("WinterHat4");
        topType.add("LongHairBigHair");
        topType.add("LongHairBob");
        topType.add("LongHairBun");
        topType.add("LongHairCurly");
        topType.add("LongHairCurvy");
        topType.add("LongHairDreads");
        topType.add("LongHairFrida");
        topType.add("LongHairFro");
        topType.add("LongHairFroBand");
        topType.add("LongHairNotTooLong");
        topType.add("LongHairShavedSides");
        topType.add("LongHairMiaWallace");
        topType.add("LongHairStraight");
        topType.add("LongHairStraight2");
        topType.add("LongHairStraightStrand");
        topType.add("ShortHairDreads01");
        topType.add("ShortHairDreads02");
        topType.add("ShortHairFrizzle");
        topType.add("ShortHairShaggyMullet");
        topType.add("ShortHairShortCurly");
        topType.add("ShortHairShortFlat");
        topType.add("ShortHairShortRound");
        topType.add("ShortHairShortWaved");
        topType.add("ShortHairSides");
        topType.add("ShortHairTheCaesar");
        topType.add("ShortHairTheCaesarSidePart");
        return topType;
    }

    public static ArrayList<String> getAccessoriesType() {
        ArrayList<String> accessoriesType = new ArrayList<>();
        accessoriesType.add("Blank");
        accessoriesType.add("Kurt");
        accessoriesType.add("Prescription01");
        accessoriesType.add("Prescription02");
        accessoriesType.add("Round");
        accessoriesType.add("Sunglasses");
        accessoriesType.add("Wayfarers");
        return accessoriesType;
    }

    public static ArrayList<String> getHairColor() {
        ArrayList<String> hairColor = new ArrayList<>();
        hairColor.add("Auburn");
        hairColor.add("Black");
        hairColor.add("Blonde");
        hairColor.add("BlondeGolden");
        hairColor.add("Brown");
        hairColor.add("BrownDark");
        hairColor.add("PastelPink");
        hairColor.add("Platinum");
        hairColor.add("Red");
        hairColor.add("SilverGray");
        return hairColor;
    }

    public static ArrayList<String> getFacialHairType() {
        ArrayList<String> facialHairType = new ArrayList<>();
        facialHairType.add("Blank");
        facialHairType.add("BeardMedium");
        facialHairType.add("BeardLight");
        facialHairType.add("BeardMajestic");
        facialHairType.add("MoustacheFancy");
        facialHairType.add("MoustacheMagnum");
        return facialHairType;
    }

    public static ArrayList<String> getFacialHairColor() {
        ArrayList<String> facialHairColor = new ArrayList<>();
        facialHairColor.add("Auburn");
        facialHairColor.add("Black");
        facialHairColor.add("Blonde");
        facialHairColor.add("BlondeGolden");
        facialHairColor.add("Brown");
        facialHairColor.add("BrownDark");
        facialHairColor.add("Platinum");
        facialHairColor.add("Red");
        return facialHairColor;
    }

    public static ArrayList<String> getClotheType() {
        ArrayList<String> clotheType = new ArrayList<>();
        clotheType.add("BlazerShirt");
        clotheType.add("BlazerSweater");
        clotheType.add("CollarSweater");
        clotheType.add("GraphicShirt");
        clotheType.add("Hoodie");
        clotheType.add("Overall");
        clotheType.add("ShirtCrewNeck");
        clotheType.add("ShirtScoopNeck");
        clotheType.add("ShirtVNeck");
        return clotheType;
    }

    public static ArrayList<String> getClotheColor() {
        ArrayList<String> clotheColor = new ArrayList<>();
        clotheColor.add("Black");
        clotheColor.add("Blue01");
        clotheColor.add("Blue02");
        clotheColor.add("Blue03");
        clotheColor.add("Gray01");
        clotheColor.add("Gray02");
        clotheColor.add("Heather");
        clotheColor.add("PastelBlue");
        clotheColor.add("PastelGreen");
        clotheColor.add("PastelOrange");
        clotheColor.add("PastelRed");
        clotheColor.add("PastelYellow");
        clotheColor.add("Pink");
        clotheColor.add("Red");
        clotheColor.add("White");
        return clotheColor;
    }

    public static ArrayList<String> getEyeType() {
        ArrayList<String> eyeType = new ArrayList<>();
        eyeType.add("Close");
        eyeType.add("Cry");
        eyeType.add("Default");
        eyeType.add("Dizzy");
        eyeType.add("EyeRoll");
        eyeType.add("Happy");
        eyeType.add("Hearts");
        eyeType.add("Side");
        eyeType.add("Squint");
        eyeType.add("Surprised");
        eyeType.add("Wink");
        eyeType.add("WinkWacky");
        return eyeType;
    }

    public static ArrayList<String> getEyebrowType() {
        ArrayList<String> eyebrowType = new ArrayList<>();
        eyebrowType.add("Angry");
        eyebrowType.add("AngryNatural");
        eyebrowType.add("Default");
        eyebrowType.add("DefaultNatural");
        eyebrowType.add("FlatNatural");
        eyebrowType.add("RaisedExcited");
        eyebrowType.add("RaisedExcitedNatural");
        eyebrowType.add("SadConcerned");
        eyebrowType.add("SadConcernedNatural");
        eyebrowType.add("UnibrowNatural");
        eyebrowType.add("UpDown");
        eyebrowType.add("UpDownNatural");
        return eyebrowType;
    }

    public static ArrayList<String> getMouthType() {
        ArrayList<String> mouthType = new ArrayList<>();
        mouthType.add("Concerned");
        mouthType.add("Default");
        mouthType.add("Disbelief");
        mouthType.add("Eating");
        mouthType.add("Grimace");
        mouthType.add("Sad");
        mouthType.add("ScreamOpen");
        mouthType.add("Serious");
        mouthType.add("Smile");
        mouthType.add("Tongue");
        mouthType.add("Twinkle");
        mouthType.add("Vomit");
        return mouthType;
    }

    public static ArrayList<String> getSkinType() {
        ArrayList<String> skinColor = new ArrayList<>();
        skinColor.add("Tanned");
        skinColor.add("Yellow");
        skinColor.add("Pale");
        skinColor.add("Light");
        skinColor.add("Brown");
        skinColor.add("DarkBrown");
        skinColor.add("Black");
        return skinColor;
    }
}
