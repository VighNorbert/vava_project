package sk.buyme.model.user;

public class PasswordPair {
    private final String password_hash;
    private final String password_salt;

    public PasswordPair(String password_hash, String password_salt) {
        this.password_hash = password_hash;
        this.password_salt = password_salt;
    }

    public String getPassword_hash() {
        return password_hash;
    }

    public String getPassword_salt() {
        return password_salt;
    }
}
