package sk.buyme.model.user;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

public class AvatarViewer {

    private final static String AVATAR_DIR = "avatars";
    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    private final static Logger logger = Logger.getLogger(AvatarViewer.class.getName());
    private static final Level LOG_LEVEL = Level.DEBUG;

    private static Image findIfExists(User user) {
        String avatar_update = sdf.format(user.getAvatar_update());
        Image img = null;
        for (File file: getMatchingImages(user.getUsername())) {
            String file_update = file.getName().substring(user.getUsername().length() + 1, file.getName().length() - 4);
            if (file_update.compareTo(avatar_update) < 0) {
                logger.setLevel(LOG_LEVEL);
                logger.debug("Deleting avatar file " + file.getName());
                file.delete();
            } else {
                img = new Image(file.toURI().toString());
            }
        }
        return img;
    }

    private static ArrayList<File> getMatchingImages(String username) {
        ArrayList<File> matchingFiles = new ArrayList<>();
        File dir = new File(Paths.get(AVATAR_DIR).toUri());
        if (!dir.exists()) {
            dir.mkdir();
            return matchingFiles;
        }
        File[] files = dir.listFiles();
        String regex = username + "-[0-9]{14}[.]png";
        if (files != null) {
            for (File file : files) {
                if (Pattern.matches(regex, file.getName())) {
                    matchingFiles.add(file);
                }
            }
        }
        return matchingFiles;
    }

    public static void showAvatar(Avatar avatar, ImageView imageView, String saveAs) {
        try {
            URL url = new URL(avatar.getImage());
            URLConnection uc = url.openConnection();
            uc.setConnectTimeout(1000);
            uc.addRequestProperty("User-Agent", "Mozilla/5.0");
            uc.connect();
            try {
                BufferedImageTranscoder transcoder = new BufferedImageTranscoder();
                InputStream file = uc.getInputStream();
                TranscoderInput transIn = new TranscoderInput(file);
                transcoder.transcode(transIn, null);
                BufferedImage bi = transcoder.getBufferedImage();
                if (saveAs != null) {
                    File outputFile = new File(Paths.get(AVATAR_DIR + "/" + saveAs).toUri());
                    ImageIO.write(bi, "png", outputFile);
                    logger.setLevel(Level.DEBUG);
                    logger.debug("Written new avatar file " + outputFile.getName());
                }
                Image img = SwingFXUtils.toFXImage(bi, null);

                imageView.setImage(img);
            } catch (TranscoderException ex) {
                ex.printStackTrace();
            }
        }
        catch (IOException io) {
            io.printStackTrace();
        }
    }

    public static void showAvatar(User user, ImageView imageView) {
        Image img = findIfExists(user);

        if (img == null) {
            showAvatar(user.getAvatar(), imageView, user.getUsername() + "-" + sdf.format(new Date()) + ".png");
        } else {
            imageView.setImage(img);
        }
    }
}
