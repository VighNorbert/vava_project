package sk.buyme.model.user;

import sk.buyme.model.address.City;
import sk.buyme.model.address.Country;
import sk.buyme.model.address.District;
import sk.buyme.model.address.Region;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class User {
    private int id;
    private String username;
    private String email;
    private String password;
    private String name;
    private String street;
    private City city;
    private District district;
    private Region region;
    private Country country;
    private Avatar avatar;
    private Date avatar_update;

    public User() {}

    public User(String username, String email, String password, String name, String street, City city) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.name = name;
        this.street = street;
        this.city = city;
    }

    public User(String username, String name) {
        this.username = username;
        this.name = name;
    }

    public User(String username, String name, Date avatar_update, Avatar avatar) {
        this.username = username;
        this.name = name;
        this.avatar_update = avatar_update;
        this.avatar = avatar;
    }

    public User(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.username = resultSet.getString("username");
        this.email = resultSet.getString("email");
        this.password = resultSet.getString("password");
        this.name = resultSet.getString("name");
        this.street = resultSet.getString("street");
        this.city = new City(resultSet.getInt("city_id"), resultSet.getString("city_name"));
        this.district = new District(resultSet.getInt("district_id"), resultSet.getString("district_name"));
        this.region = new Region(resultSet.getInt("region_id"), resultSet.getString("region_name"));
        this.country = new Country(resultSet.getInt("country_id"), resultSet.getString("country_name"));
        this.avatar_update = resultSet.getTimestamp("d_avatar_update");
        //if (resultSet.getString("top_type") != null) {
        this.avatar = new Avatar(resultSet);
        //}
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public District getDistrict() {
        return district;
    }

    public Region getRegion() {
        return region;
    }

    public Country getCountry() {
        return country;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public Date getAvatar_update() {
        return avatar_update;
    }

    public void setAvatarUpdate() {
        this.avatar_update = new Date();
    }
}
