package sk.buyme.model.user;

import javafx.scene.Parent;
import sk.buyme.model.store.Store;
import sk.buyme.model.store.StoreChain;

public class Application {
    private static Application instance;

    private User loggedInUser = null;

    private StoreChain selectedStoreChain = null;

    private Store selectedStore = null;

    /**
     * Used for changing style of parent when new store chain is selected
     */
    private Parent selectedStoreChainParent = null;

    private Application() {
    }

    public static Application getInstance() {
        if(instance==null)
            instance = new Application();
        return instance;
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public StoreChain getSelectedStoreChain() {
        return selectedStoreChain;
    }

    public void setSelectedStoreChain(StoreChain selectedStoreChain) {
        this.selectedStoreChain = selectedStoreChain;
    }

    public Parent getSelectedStoreChainParent() {
        return selectedStoreChainParent;
    }

    public void setSelectedStoreChainParent(Parent selectedStoreChainParent) {
        this.selectedStoreChainParent = selectedStoreChainParent;
    }

    public Store getSelectedStore() {
        return selectedStore;
    }

    public void setSelectedStore(Store selectedStore) {
        this.selectedStore = selectedStore;
    }
}
