package sk.buyme.model.user;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Avatar {
    private String topType;
    private String accessoriesType;
    private String hairColor;
    private String facialHairType;
    private String facialHairColor;
    private String clotheType;
    private String clotheColor;
    private String eyeType;
    private String eyebrowType;
    private String mouthType;
    private String skinColor;

    public Avatar(
            String topType,
            String accessoriesType,
            String hairColor,
            String facialHairType,
            String facialHairColor,
            String clotheType,
            String clotheColor,
            String eyeType,
            String eyebrowType,
            String mouthType,
            String skinColor) {
        this.topType = topType;
        this.accessoriesType = accessoriesType;
        this.hairColor = hairColor;
        this.facialHairType = facialHairType;
        this.facialHairColor = facialHairColor;
        this.clotheType = clotheType;
        this.clotheColor = clotheColor;
        this.eyeType = eyeType;
        this.eyebrowType = eyebrowType;
        this.mouthType = mouthType;
        this.skinColor = skinColor;
    }

    public Avatar(ResultSet resultSet) throws SQLException {
        this.topType = resultSet.getString("top_type");
        this.accessoriesType = resultSet.getString("accessories_type");
        this.hairColor = resultSet.getString("hair_color");
        this.facialHairType = resultSet.getString("facial_hair_type");
        this.facialHairColor = resultSet.getString("facial_hair_color");
        this.clotheType = resultSet.getString("clothe_type");
        this.clotheColor = resultSet.getString("clothe_color");
        this.eyeType = resultSet.getString("eye_type");
        this.eyebrowType = resultSet.getString("eyebrow_type");
        this.mouthType = resultSet.getString("mouth_type");
        this.skinColor = resultSet.getString("skin_color");
    }

    public String getImage() {
        return "https://avataaars.io/?avatarStyle=Circle" +
                "&topType=" + topType +
                "&accessoriesType=" + accessoriesType +
                "&hairColor=" + hairColor +
                "&facialHairType=" + facialHairType +
                "&facialHairColor=" + facialHairColor +
                "&clotheType=" + clotheType +
                "&clotheColor=" + clotheColor +
                "&eyeType=" + eyeType +
                "&eyebrowType=" + eyebrowType +
                "&mouthType=" + mouthType +
                "&skinColor=" + skinColor;
    }

    public String getTopType() {
        return topType;
    }

    public void setTopType(String topType) {
        this.topType = topType;
    }

    public String getAccessoriesType() {
        return accessoriesType;
    }

    public void setAccessoriesType(String accessoriesType) {
        this.accessoriesType = accessoriesType;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getFacialHairType() {
        return facialHairType;
    }

    public void setFacialHairType(String facialHairType) {
        this.facialHairType = facialHairType;
    }

    public String getFacialHairColor() {
        return facialHairColor;
    }

    public void setFacialHairColor(String facialHairColor) {
        this.facialHairColor = facialHairColor;
    }

    public String getClotheType() {
        return clotheType;
    }

    public void setClotheType(String clotheType) {
        this.clotheType = clotheType;
    }

    public String getClotheColor() {
        return clotheColor;
    }

    public void setClotheColor(String clotheColor) {
        this.clotheColor = clotheColor;
    }

    public String getEyeType() {
        return eyeType;
    }

    public void setEyeType(String eyeType) {
        this.eyeType = eyeType;
    }

    public String getEyebrowType() {
        return eyebrowType;
    }

    public void setEyebrowType(String eyebrowType) {
        this.eyebrowType = eyebrowType;
    }

    public String getMouthType() {
        return mouthType;
    }

    public void setMouthType(String mouthType) {
        this.mouthType = mouthType;
    }

    public String getSkinColor() {
        return skinColor;
    }

    public void setSkinColor(String skinColor) {
        this.skinColor = skinColor;
    }
}
