package sk.buyme.model.window;

public enum WindowType {
    ALERT_WINDOW,
    CONFIRM_WINDOW
}
