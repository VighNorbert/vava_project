package sk.buyme.model.window;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sk.buyme.controller.surrouding.ViewManager;
import sk.buyme.controller.surrouding.WindowController;

import java.io.IOException;

public class Window {
    private static boolean answer = false;

    /**
     * Static method to display simple window messages on current stage
     * @param title Title of window message
     * @param message Content of window message
     * @param windowType type of window to be displayed, possible values are WindowType.ALERT_WINDOW
     *                  which is simple alert message or WindowType.CONFIRM_WINDOW which is confirmation window
     * @return boolean value whether confirm button was pressed or not
     */
    public static boolean display(String title, String message, WindowType windowType){
        try {
            ColorAdjust colorAdjust = new ColorAdjust();
            colorAdjust.setBrightness(-0.4);
            colorAdjust.setContrast(-0.2);
            ViewManager.getInstance().getBorderPane().setEffect(colorAdjust);

            FXMLLoader loader = new FXMLLoader(Window.class.getResource("/sk/buyme/view/surrouding/Window.fxml"));
            Parent window = (Parent) loader.load();
            WindowController controller = loader.getController();

            controller.setData(title, message);

            Stage popupStage = new Stage(StageStyle.TRANSPARENT);
            double positionX = ViewManager.getInstance().getPrimaryStage().getX() + 435;
            double positionY = ViewManager.getInstance().getPrimaryStage().getY() + 305;
            popupStage.setX(positionX);
            popupStage.setY(positionY);

            popupStage.initOwner(ViewManager.getInstance().getPrimaryStage());
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setScene(new Scene(window, Color.TRANSPARENT));

            controller.getCloseButton().setOnAction(e ->{
                answer = false;
                ViewManager.getInstance().getBorderPane().setEffect(null);
                popupStage.hide();
            });

            controller.getOkButton().setOnAction(e ->{
                answer = true;
                ViewManager.getInstance().getBorderPane().setEffect(null);
                popupStage.hide();
            });

            if(windowType.equals(WindowType.CONFIRM_WINDOW)){
                controller.getOkButton().setText("Áno");
                Button noButton = new Button("Nie");
                noButton.setPrefWidth(200);
                noButton.setPrefHeight(40);
                noButton.setStyle("-fx-background-color: #515151");
                noButton.setCursor(Cursor.HAND);
                noButton.setOnAction(e->{
                    answer = false;
                    ViewManager.getInstance().getBorderPane().setEffect(null);
                    popupStage.hide();

                });

                HBox buttonHBox = controller.getButtonHbox();
                HBox.setMargin(noButton, new Insets(0, 0, 0, 50));

                buttonHBox.getChildren().add(noButton);

            }

            popupStage.showAndWait();

            return answer;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return answer;
    }
}
