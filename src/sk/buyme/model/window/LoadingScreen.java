package sk.buyme.model.window;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sk.buyme.controller.surrouding.ViewManager;

public class LoadingScreen {
    private Stage stage;

    private ColorAdjust colorAdjust;
    private VBox window;

    public void init(){
        colorAdjust = new ColorAdjust();
        colorAdjust.setBrightness(-0.4);
        colorAdjust.setContrast(-0.2);
        window = new VBox();

        window.setAlignment(Pos.CENTER);
        ProgressIndicator indicator = new ProgressIndicator();
        indicator.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        window.getChildren().add(indicator);

        stage = new Stage(StageStyle.TRANSPARENT);
        double positionX = ViewManager.getInstance().getPrimaryStage().getX() + 545;
        double positionY = ViewManager.getInstance().getPrimaryStage().getY() + 390;
        stage.setX(positionX);
        stage.setY(positionY);

        stage.initOwner(ViewManager.getInstance().getPrimaryStage());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(window, Color.TRANSPARENT));
    }

    public void startLoading(){
        ViewManager.getInstance().getBorderPane().setEffect(colorAdjust);
//        window.setStyle("-fx-background-color: transparent");
        stage.show();

    }

    public void stopLoading(){
        ViewManager.getInstance().getBorderPane().setEffect(null);
        stage.hide();
    }
}
