package sk.buyme.model.store;

import sk.buyme.model.address.City;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Store {
    private  int id;
    private final String name;
    private  City city;
    private  String address;
    private  StoreChain storeChain;

    public Store(ResultSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.name = rs.getString("name");
        this.city = new City(rs.getInt("city_id"), rs.getString("city_name"));
        this.address = rs.getString("address");
        this.storeChain = new StoreChain(rs.getInt("storechain_id"), rs.getString("storechain_name"), rs.getString("storechain_img"));
    }

    public Store (String name) {
        this.name = name;
    }

    public Store(int id, String name, City city, String address, StoreChain storeChain) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.address = address;
        this.storeChain = storeChain;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public City getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public StoreChain getStoreChain() {
        return storeChain;
    }

    @Override
    public String toString() {
        return
                storeChain + ", " +
                name + " \n" +
                address +  ", " + city;
    }
}
