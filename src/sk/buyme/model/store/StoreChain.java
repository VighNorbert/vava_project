package sk.buyme.model.store;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StoreChain {
    private final int id;
    private final String name;
    private final String img;

    public StoreChain(int id, String name, String img) {
        this.id = id;
        this.name = name;
        this.img = img;
    }

    public StoreChain(ResultSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.name = rs.getString("name");
        this.img = rs.getString("logo_img");
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImg() {
        return img;
    }

    @Override
    public String toString() {
        return name;
    }
}
