package sk.buyme.model.store;

import sk.buyme.model.Model;
import sk.buyme.model.address.District;
import sk.buyme.model.user.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StoreModel extends Model {
    private static StoreModel instance = null;

    private ArrayList<StoreChain> storeChains = null;

    private StoreModel() {}

    public static StoreModel getInstance() {
        if (instance == null) {
            instance = new StoreModel();
        }
        return instance;
    }

    public ArrayList<StoreChain> getAllStoreChains() {
        if (storeChains == null) {
            storeChains = new ArrayList<>();
            try (Connection conn = getConnection()) {
                PreparedStatement ps = conn.prepareStatement("SELECT storechain.* FROM storechain");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    StoreChain sc = new StoreChain(rs);
                    storeChains.add(sc);
                }
                getLogger().info("Returned all store-chains");
            } catch (SQLException ex) {
                getLogger().fatal("Could not connect to database", ex);
            }
        }
        return storeChains;
    }

    public StoreChain getStoreChain(String name) {
        for (StoreChain storeChain : this.getAllStoreChains()) {
            if (storeChain.getName().equals(name))
                return storeChain;
        }
        return null;
    }

    public StoreChain getStoreChain(int id) {
        for (StoreChain storeChain : this.getAllStoreChains()) {
            if (storeChain.getId() == id)
                return storeChain;
        }
        return null;
    }

    public ArrayList<Store> getStores(StoreChain storeChain) {
        ArrayList<Store> stores = new ArrayList<>();
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                "SELECT store.*, c.name as city_name, s.name as storechain_name, s.logo_img as storechain_img " +
                "FROM store " +
                "JOIN city c on store.city_id = c.id " +
                "JOIN storechain s on store.storechain_id = s.id WHERE s.id = ?");
            ps.setInt(1, storeChain.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Store s = new Store(rs);
                stores.add(s);
            }
            getLogger().info("Returned all stores");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return stores;
    }

    public ArrayList<Store> getStores(StoreChain storeChain, District district) {
        ArrayList<Store> stores = new ArrayList<>();
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                "SELECT store.*, c.name as city_name, s.name as storechain_name, s.logo_img as storechain_img " +
                "FROM store " +
                "JOIN city c on store.city_id = c.id " +
                "JOIN storechain s on store.storechain_id = s.id " +
                "JOIN district d on c.district_id = d.id " +
                "WHERE s.id = ? and d.id = ?");
            ps.setInt(1, storeChain.getId());
            ps.setInt(2, district.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Store s = new Store(rs);
                stores.add(s);
            }
            getLogger().info("Returned all stores in district " + district.getName());
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return stores;
    }

    public ArrayList<Store> getStores(StoreChain storeChain, User user) {
        return this.getStores(storeChain, user.getDistrict());
    }
}
