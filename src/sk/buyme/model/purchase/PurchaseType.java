package sk.buyme.model.purchase;

public enum PurchaseType {
    SHOPPING_LIST,
    SHOPPING_LIST_ACCEPTED,
    SHOPPING_CHECKLIST,
    SHOPPING_OFFER
}
