package sk.buyme.model.purchase;

public enum PurchaseSortBy {
    PRICE_ASC,
    PRICE_DESC,
    DATE_ASC,
    DATE_DESC
}

