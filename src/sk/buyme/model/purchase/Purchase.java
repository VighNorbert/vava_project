package sk.buyme.model.purchase;

import sk.buyme.model.address.City;
import sk.buyme.model.products.ShoppingCartItem;
import sk.buyme.model.store.Store;
import sk.buyme.model.store.StoreModel;
import sk.buyme.model.user.User;
import sk.buyme.model.user.Avatar;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Purchase {
    private int id;
    private final User customer;
    private User buyer;
    private final Store store;
    private final double estimatedPrice;
    private final double estimatedProfit;
    private double realPrice;
    private double realProfit;
    private boolean isFinished;
    private ArrayList<ShoppingCartItem> shoppingCartItems;
    private final String street;
    private final City city;
    private int status;

    public Purchase(User customer, Store store, double estimatedPrice, double estimatedProfit, String street, City city) {
        this.customer = customer;
        this.store = store;
        this.estimatedPrice = estimatedPrice;
        this.estimatedProfit = estimatedProfit;
        this.street = street;
        this.city = city;
        this.shoppingCartItems = new ArrayList<>();
    }

    public Purchase(User customer, User buyer, Store store, double estimatedPrice, double estimatedProfit, String street, City city) {
        this.customer = customer;
        this.buyer = buyer;
        this.store = store;
        this.estimatedPrice = estimatedPrice;
        this.estimatedProfit = estimatedProfit;
        this.street = street;
        this.city = city;
        this.shoppingCartItems = new ArrayList<>();
    }

    public Purchase(User customer, Store store, double estimatedPrice, double estimatedProfit, ArrayList<ShoppingCartItem> shoppingCartItems, String street, City city) {
        this.customer = customer;
        this.store = store;
        this.estimatedPrice = estimatedPrice;
        this.estimatedProfit = estimatedProfit;
        this.shoppingCartItems = shoppingCartItems;
        this.street = street;
        this.city = city;
    }

    public Purchase(User customer, User buyer, Store store, double estimatedPrice, double estimatedProfit, ArrayList<ShoppingCartItem> shoppingCartItems, String street, City city) {
        this.customer = customer;
        this.buyer = buyer;
        this.store = store;
        this.estimatedPrice = estimatedPrice;
        this.estimatedProfit = estimatedProfit;
        this.shoppingCartItems = shoppingCartItems;
        this.street = street;
        this.city = city;
    }

    public Purchase(ResultSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.customer = new User(
                rs.getString("customer_username"),
                rs.getString("customer_name"),
                rs.getDate("customer_d_avatar_update"),
                new Avatar(rs));
        if (rs.getString("buyer_username") != null) {
            this.buyer = new User(
                    rs.getString("buyer_username"),
                    rs.getString("buyer_name"),
                    rs.getDate("buyer_d_avatar_update"),
                    new Avatar(rs));
        }
        this.store = new Store(
                rs.getInt("store_id"),
                rs.getString("store_name"),
                new City(
                    rs.getInt("store_city_id"),
                    rs.getString("store_city_name")),
                rs.getString("store_address"),
                StoreModel.getInstance().getStoreChain(rs.getInt("storechain_id"))
        );
        this.estimatedPrice = rs.getDouble("estimated_price");
        this.estimatedProfit = rs.getDouble("estimated_profit");
        this.street = rs.getString("street");
        this.city = new City(rs.getInt("city_id"), rs.getString("city_name"));
        this.realPrice = rs.getDouble("real_price");
        this.realProfit = rs.getDouble("real_profit");
        this.shoppingCartItems = new ArrayList<>();
        this.status = rs.getInt("status");
    }

    public int getId() {
        return id;
    }

    public User getCustomer() {
        return customer;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    @Deprecated
    public double getNameKm() {
        return 0d;
    }

    public Store getStore() {
        return store;
    }

    @Deprecated
    public double getShopKm() {
        return 0d;
    }

    public Double getEstimatedPrice() {
        return estimatedPrice;
    }

    public double getEstimatedProfit() {
        return estimatedProfit;
    }

    public double getRealPrice() {
        return realPrice;
    }

    public double getRealProfit() {
        return realProfit;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void finish(double realPrice, double realProfit) {
        this.isFinished = true;
        this.realPrice = realPrice;
        this.realProfit = realProfit;
    }

    public void emptyProducts() {
        shoppingCartItems = new ArrayList<>();
    }

    public void addProduct(ShoppingCartItem shoppingCartItem) {
        shoppingCartItems.add(shoppingCartItem);
    }

    public void removeProduct(ShoppingCartItem shoppingCartItem) {
        shoppingCartItems.remove(shoppingCartItem);
    }

    public ArrayList<ShoppingCartItem> getProducts() {
        return (ArrayList<ShoppingCartItem>) shoppingCartItems.clone();
    }

    public String getStreet() {
        return street;
    }

    public City getCity() {
        return city;
    }

    public int getStatus() {
        return status;
    }
}
