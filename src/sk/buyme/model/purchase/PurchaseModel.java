package sk.buyme.model.purchase;

import sk.buyme.model.Model;
import sk.buyme.model.products.ShoppingCartItem;
import sk.buyme.model.user.User;

import java.sql.*;
import java.util.ArrayList;

public class PurchaseModel extends Model {
    public ArrayList<Purchase> getUnassigned(PurchaseSortBy sortBy, User loggedIn) {
        ArrayList<Purchase> purchases = new ArrayList<>();
        try (Connection conn = getConnection()) {
            String order = "";
            switch (sortBy) {
                case DATE_ASC:
                    order = "d_create ASC";
                    break;
                case PRICE_ASC:
                    order = "estimated_price ASC";
                    break;
                case PRICE_DESC:
                    order = "estimated_price DESC";
                    break;
                default:
                    order = "d_create DESC";
            }
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT " +
                            "purchase.*, " +
                            "city.name as city_name, " +
                            "customer.username as customer_username, " +
                            "customer.name as customer_name, " +
                            "customer.d_avatar_update as customer_d_avatar_update, " +
                            "customer.top_type, " +
                            "customer.accessories_type, " +
                            "customer.hair_color, " +
                            "customer.facial_hair_type, " +
                            "customer.facial_hair_color, " +
                            "customer.clothe_type, " +
                            "customer.clothe_color, " +
                            "customer.eye_type, " +
                            "customer.eyebrow_type, " +
                            "customer.mouth_type, " +
                            "customer.skin_color, " +
                            "null as buyer_username, " +
                            "null as buyer_name, " +
                            "null as buyer_d_avatar_update, " +
                            "store.name as store_name, " +
                            "store.city_id as store_city_id, " +
                            "store_city.name as store_city_name, " +
                            "store.address as store_address, " +
                            "store.storechain_id " +
                            "from purchase " +
                            "JOIN store on store.id = purchase.store_id " +
                            "JOIN city on purchase.city_id = city.id " +
                            "JOIN city as store_city on store.city_id = store_city.id " +
                            "JOIN users as customer on customer.id = purchase.customer_id " +
                            "WHERE purchase.status = 1 and customer_id != ? " +
                            "ORDER BY " + order
            );
            ps.setInt(1, loggedIn.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Purchase p = new Purchase(rs);
                purchases.add(p);
            }
            getLogger().info("Returned all unassigned purchases");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return purchases;
    }

    public ArrayList<Purchase> getByBuyer(User buyer, boolean actual) {
        ArrayList<Purchase> purchases = new ArrayList<>();
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT " +
                            "purchase.*, " +
                            "city.name as city_name, " +
                            "customer.username as customer_username, " +
                            "customer.name as customer_name, " +
                            "customer.d_avatar_update as customer_d_avatar_update, " +
                            "customer.top_type, " +
                            "customer.accessories_type, " +
                            "customer.hair_color, " +
                            "customer.facial_hair_type, " +
                            "customer.facial_hair_color, " +
                            "customer.clothe_type, " +
                            "customer.clothe_color, " +
                            "customer.eye_type, " +
                            "customer.eyebrow_type, " +
                            "customer.mouth_type, " +
                            "customer.skin_color, " +
                            "buyer.username as buyer_username, " +
                            "buyer.name as buyer_name, " +
                            "buyer.d_avatar_update as buyer_d_avatar_update, " +
                            "store.name as store_name, " +
                            "store.city_id as store_city_id, " +
                            "store_city.name as store_city_name, " +
                            "store.address as store_address, " +
                            "store.storechain_id " +
                        "from purchase " +
                        "JOIN store on store.id = purchase.store_id " +
                        "JOIN city on purchase.city_id = city.id " +
                        "JOIN city as store_city on store.city_id = store_city.id " +
                        "JOIN users as customer on customer.id = purchase.customer_id " +
                        "JOIN users as buyer on buyer.id = purchase.buyer_id " +
                        "WHERE purchase.buyer_id = ? AND purchase.status = " + (actual ? "2 " : "3 ") +
                        "ORDER BY purchase.d_create DESC"
            );
            ps.setInt(1, buyer.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Purchase p = new Purchase(rs);
                purchases.add(p);
            }
            getLogger().info("Returned all purchases made by buyer " + buyer.getName());
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return purchases;
    }

    public ArrayList<Purchase> getByCustomer(User customer, boolean actual) {
        ArrayList<Purchase> purchases = new ArrayList<>();
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT " +
                            "purchase.*, " +
                            "city.name as city_name, " +
                            "customer.username as customer_username, " +
                            "customer.name as customer_name, " +
                            "customer.d_avatar_update as customer_d_avatar_update, " +
                            "buyer.username as buyer_username, " +
                            "buyer.name as buyer_name, " +
                            "buyer.d_avatar_update as buyer_d_avatar_update, " +
                            "buyer.top_type, " +
                            "buyer.accessories_type, " +
                            "buyer.hair_color, " +
                            "buyer.facial_hair_type, " +
                            "buyer.facial_hair_color, " +
                            "buyer.clothe_type, " +
                            "buyer.clothe_color, " +
                            "buyer.eye_type, " +
                            "buyer.eyebrow_type, " +
                            "buyer.mouth_type, " +
                            "buyer.skin_color, " +
                            "store.name as store_name, " +
                            "store.city_id as store_city_id, " +
                            "store_city.name as store_city_name, " +
                            "store.address as store_address, " +
                            "store.storechain_id " +
                        "from purchase " +
                        "JOIN store on store.id = purchase.store_id " +
                        "JOIN city on purchase.city_id = city.id " +
                        "JOIN city as store_city on store.city_id = store_city.id " +
                        "JOIN users as customer on customer.id = purchase.customer_id " +
                        "LEFT JOIN users as buyer on buyer.id = purchase.buyer_id " +
                        "WHERE purchase.customer_id = ? AND purchase.status " + (actual ? "< 4 " : " > 3 ") +
                        "ORDER BY purchase.d_create DESC"
            );
            ps.setInt(1, customer.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Purchase p = new Purchase(rs);
                purchases.add(p);
            }
            getLogger().info("Returned all purchases of customer " + customer.getName());
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
        return purchases;
    }

    public void loadProducts(Purchase purchase) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT " +
                            "pp.id as pp_id, " +
                            "pp.*, " +
                            "p.* " +
                        "from purchase_products as pp " +
                        "JOIN product as p on pp.product_id = p.id " +
                        "WHERE pp.purchase_id = ?"
            );
            ps.setInt(1, purchase.getId());
            ResultSet rs = ps.executeQuery();
            purchase.emptyProducts();
            while (rs.next()) {
                ShoppingCartItem sci = new ShoppingCartItem(rs);
                purchase.addProduct(sci);
            }
            getLogger().info("Loaded products associated with purchase");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }

    public void save(Purchase purchase) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO purchase (customer_id, store_id, estimated_price, estimated_profit, street, city_id) " +
                            "VALUES (?, ?, ?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS
            );
            ps.setInt(1, purchase.getCustomer().getId());
            ps.setInt(2, purchase.getStore().getId());
            ps.setDouble(3, purchase.getEstimatedPrice());
            ps.setDouble(4, purchase.getEstimatedProfit());
            ps.setString(5, purchase.getStreet());
            ps.setInt(6, purchase.getCity().getId());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int purchase_id = rs.getInt(1);
            String sql = "INSERT INTO purchase_products (purchase_id, product_id, quantity) VALUES\n";
            for (ShoppingCartItem sci : purchase.getProducts()) {
                sql = sql.concat("(" + purchase_id + ", " + sci.getProduct().getId() + ", " + sci.getQuantity() + "),\n");
            }
            ps = conn.prepareStatement(sql.substring(0, sql.length() - 2));
            ps.executeUpdate();
            getLogger().info("Inserted new purchase");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }

    public void assignBuyer(Purchase purchase) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("UPDATE purchase SET buyer_id = ?, status = 2, d_update = now() WHERE id = ?");
            ps.setInt(1, purchase.getBuyer().getId());
            ps.setInt(2, purchase.getId());
            ps.executeUpdate();
            getLogger().info("Purchase id " + purchase.getId() + " was updated");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }

    public void remove(Purchase purchase) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("UPDATE purchase SET status = 5, d_update = now() WHERE id = ?");
            ps.setInt(1, purchase.getId());
            ps.executeUpdate();
            getLogger().info("Purchase id " + purchase.getId() + " was updated");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }

    public void setBought(Purchase purchase) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("UPDATE purchase SET real_price = ?, real_profit = ?, status = 3, d_update = now() WHERE id = ?");
            ps.setDouble(1, purchase.getRealPrice());
            ps.setDouble(2, purchase.getRealProfit());
            ps.setInt(3, purchase.getId());
            ps.executeUpdate();

            String sql = "INSERT INTO purchase_products (id, purchase_id, product_id, quantity, available) VALUES\n";
            for (ShoppingCartItem sci : purchase.getProducts()) {
                sql = sql.concat("(" + sci.getId() + ", " + purchase.getId() + ", " + sci.getProduct().getId() + ", " + sci.getQuantity() + ", " + (sci.isAvailable() == null ? "null" : sci.isAvailable().toString()) + "),\n");
            }
            ps = conn.prepareStatement(sql.substring(0, sql.length() - 2) +
                    " ON CONFLICT (id) DO UPDATE set available = EXCLUDED.available");
            ps.executeUpdate();

            getLogger().info("Purchase id " + purchase.getId() + " was updated");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }

    public void saveProductsState(Purchase purchase) {
        try (Connection conn = getConnection()) {
            String sql = "INSERT INTO purchase_products (id, purchase_id, product_id, quantity, available) VALUES\n";
            for (ShoppingCartItem sci : purchase.getProducts()) {
                sql = sql.concat("(" + sci.getId() + ", " + purchase.getId() + ", " + sci.getProduct().getId() + ", " + sci.getQuantity() + ", " + (sci.isAvailable() == null ? "null" : sci.isAvailable().toString()) + "),\n");
            }
            PreparedStatement ps = conn.prepareStatement(sql.substring(0, sql.length() - 2) +
                    " ON CONFLICT (id) DO UPDATE set available = EXCLUDED.available");
            ps.executeUpdate();

            getLogger().info("Purchase id " + purchase.getId() + " was updated");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }

    public void setDelivered(Purchase purchase) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("UPDATE purchase SET status = 4, d_update = now() WHERE id = ?");
            ps.setInt(1, purchase.getId());
            ps.executeUpdate();
            getLogger().info("Purchase id " + purchase.getId() + " was updated");
        } catch (SQLException ex) {
            getLogger().fatal("Could not connect to database", ex);
        }
    }
}
